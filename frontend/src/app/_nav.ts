import { INavData } from '@coreui/angular';

export const navAdminItems: INavData[] = [
  // {
  //   name: 'Dashboard',
  //   url: '/dashboard',
  //   icon: 'icon-speedometer'
  // },
  {
    name: 'Stock',
    url: '/stock',
    icon: 'fa fa-cubes'
  },
  {
    name: 'Dispatch',
    url: '/dispatch',
    icon: 'fa fa-cubes'
  },
  {
    name: 'Agent',
    url: '/agent',
    icon: 'fa fa-group'
  },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-user'
  },
  {
    name: 'Area',
    url: '/area',
    icon: 'icon-home'
  },
  {
    name: 'Reports',
    url: '/reports',
    icon: 'fa fa-sticky-note'
  }
];
export const navItems: INavData[] = [
  // {
  //   name: 'Dashboard',
  //   url: '/dashboard',
  //   icon: 'icon-speedometer'
  // },

  // {
  //   name: 'Users',
  //   url: '/users',
  //   icon: 'icon-user',
  //   variant:'Admin'
  // },
  // {
  //   name: 'Area',
  //   url: '/area',
  //   icon: 'icon-home',
  //   variant:'Admin'
  // },
  {
    name: 'Stock',
    url: '/stock',
    icon: 'fa fa-cubes'
  },
  {
    name: 'Dispatch',
    url: '/dispatch',
    icon: 'fa fa-cubes'
  },
  {
    name: 'Agent',
    url: '/agent',
    icon: 'fa fa-group'
  }
];
