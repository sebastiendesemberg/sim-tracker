export class User {
    active: boolean;
    createdDate: any;
    lastUpdatedDate: any;
    _id: any;
    name: any;
    cell: any;
    email: any;
    password: any;
    area: any;
    bankName: any;
    role: any;
    surName: any;
}