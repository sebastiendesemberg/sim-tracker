export const constant = {
  // User Routes
  LOGIN: 'user/login',
  SIGNUP: 'user/signup',
  VERIFY_USER_EMAIL: 'user/verify/email',
  VERIFY_EMAIL_AFTER_SIGNUP: 'user/verify',
  FORGOTPASSWORD: 'user/recoverPassword',
  VERIFYEMAILPASSWORD: 'user/verifyPasswordEmail',
  RESETPASSWORD: 'user/resetPassword',
  VERIFY_TOKEN: 'user/verifyToken',
  UPDATE_USER: 'user/updateUser',
  UPDATE_ADMIN: 'user/updateAdmin',
  GET_USER_DETAIL: 'user/getSingleUser',
  REMOVE_IMAGE: 'user/removeImage',
  CHECK_CURRENT_PASS: 'user/checkpass',
  UPDATE_PASS: 'user/updatepassword',
  SUBSCRIBE_USER: 'user/subscribe',
  MAIL_SEND_DATA_UPDATE: 'user/updateMailData',
  MAIL_GET: 'user/mailGet',


};
