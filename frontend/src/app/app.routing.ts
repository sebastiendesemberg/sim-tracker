import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Import Containers
import { DefaultLayoutComponent } from './views/containers';
import { P404Component } from './views/error/404.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './services/guard';
import { ForgotpasswordComponent } from './views/forgotpassword/forgotpassword.component';

export const routes: Routes = [

  { path: '', redirectTo: 'login', pathMatch: 'full', },
  { path: '404', component: P404Component, data: { title: 'Page 404' } },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuard] },

  { path: 'forgot-password', component: ForgotpasswordComponent },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Users' },
    canActivate: [AuthGuard],
    children: [{
      path: 'users',
      loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Area' },
    canActivate: [AuthGuard],
    children: [{
      path: 'area',
      loadChildren: () => import('./views/area/area.module').then(m => m.AreaModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Agent' },
    canActivate: [AuthGuard],
    children: [{
      path: 'agent',
      loadChildren: () => import('./views/agent/agent.module').then(m => m.AgentModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Stock' },
    canActivate: [AuthGuard],
    children: [{
      path: 'stock',
      loadChildren: () => import('./views/stock/stock.module').then(m => m.StockModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Dispatch' },
    canActivate: [AuthGuard],
    children: [{
      path: 'dispatch',
      loadChildren: () => import('./views/dispatch/dispatch.module').then(m => m.DispatchModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Brick' },
    canActivate: [AuthGuard],
    children: [{
      path: 'brick',
      loadChildren: () => import('./views/brick/brick.module').then(m => m.BrickModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Pack' },
    canActivate: [AuthGuard],
    children: [{
      path: 'pack',
      loadChildren: () => import('./views/pack/pack.module').then(m => m.PackModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Reports' },
    canActivate: [AuthGuard],
    children: [{
      path: 'reports',
      loadChildren: () => import('./views/reports/reports.module').then(m => m.ReportsModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Add-box' },
    canActivate: [AuthGuard],
    children: [{
      path: 'add-box',
      loadChildren: () => import('./views/agent-box/agent-box.module').then(m => m.AgentBoxModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'View-boxes' },
    canActivate: [AuthGuard],
    children: [{
      path: 'view-boxes',
      loadChildren: () => import('./views/view-boxes/view-boxes.module').then(m => m.ViewBoxesModule)
    }]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: { title: 'Welcome-page' },
    canActivate: [AuthGuard],
    children: [{
      path: 'welcome-page',
      loadChildren: () => import('./views/welcome-page/welcome-page.module').then(m => m.WelcomePageModule)
    }]
  },
  {
    path: '**',
    component: P404Component

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
