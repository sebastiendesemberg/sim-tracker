import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from '../agent/ng-table-module';
import { Daterangepicker } from 'ng2-daterangepicker';
import { NgxConfirmBoxModule, NgxConfirmBoxService } from 'ngx-confirm-box';
@NgModule({
  declarations: [ReportsComponent],
  imports: [
    ReportsRoutingModule,
    FormsModule,
	NgxConfirmBoxModule,
    PaginationModule.forRoot(),
    Ng2TableModule,
    Daterangepicker,
    CommonModule
	],
	providers: [
		NgxConfirmBoxService
	]
})
export class ReportsModule {}
