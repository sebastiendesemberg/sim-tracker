import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as XLSX from 'xlsx';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable'
import { autoTable as AutoTable } from 'jspdf-autotable'
import * as moment from 'moment';
import { NgxConfirmBoxService } from 'ngx-confirm-box';

const base_url = environment.apiUrl;
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  data: Array < any > = [];
  reportType: string = 'area';

  returnedArray: Array < any > = [];
  commissionTable = '';
  monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  user: any;
  countofreport = 0;
  totlength: any;
  countYears = 1970;
  currentFullYear = new Date().getFullYear();
  yearList = [];
  selectedMonth: any = new Date().getMonth() + 1;
  formData: FormData;
  file: File;
  selectedYear: any = new Date().getFullYear();
  selectedMonthVal: any = (new Date().getMonth() + 1) <= 9 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1);
  fromSelectedMonthVal: any = (new Date().getMonth() + 1) <= 9 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1);
  toSelectedMonthVal: any = (new Date().getMonth() + 1) <= 9 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1);
  fromSelectedYear: any = new Date().getFullYear();
  toSelectedYear: any = new Date().getFullYear();
  fromSelectedMonth: any = new Date().getMonth() + 1;
  toSelectedMonth: any = new Date().getMonth() + 1;

  bgColor = 'rgba(0,0,0,0.5)'; // overlay background color
  confirmHeading = '';
  confirmContent = "Are you sure, want to delete this?";
  confirmCanceltext = "Cancel";
  confirmOkaytext = "Okay";

  constructor(public router: Router,
    private toastr: ToastrService,
    private httpService: HttpClient,
    private confirmBox: NgxConfirmBoxService,
    public auth: AuthService) {
    // console.log(this.selectedMonth);

    while (this.countYears != this.currentFullYear) {
      this.countYears++;
      this.user = this.auth.currentUserValue;
      this.yearList.push(this.countYears);
    }

  }

  ngOnInit(): void {

  }


  public daterange: any = {};

  // see original project for full list of options
  // can also be setup using the config service to apply to multiple pickers
  public options: any = {
    locale: { format: 'YYYY-MM-DD' },
    alwaysShowCalendars: false,
  };

  filterMonthChange(value) {
    console.log(value);
    this.selectedMonthVal = (value <= 9 ? '0' + value : value);

    // console.log(this.selectedMonth);
  }
  filterYearChange(value) {
    console.log(value);
    this.selectedYear = value;
    // console.log(this.selectedMonth);
  }
  filterFromMonthChange(value) {
    console.log(value);
    this.fromSelectedMonthVal = (value <= 9 ? '0' + value : value);

    // console.log(this.selectedMonth);
  }
  filterFromYearChange(value) {
    console.log(value);
    this.fromSelectedYear = value;
    // console.log(this.selectedMonth);
  }
  filterToMonthChange(value) {
    console.log(value);
    this.toSelectedMonthVal = (value <= 9 ? '0' + value : value);

    // console.log(this.selectedMonth);
  }
  filterToYearChange(value) {
    console.log(value);
    this.toSelectedYear = value;
    // console.log(this.selectedMonth);
  }



  public selectedDate(event, datepicker ? : any) {
    // this is the date  selected
    console.log(event);

    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = event.start;
    datepicker.end = event.end;

    // use passed valuable to update state
    this.daterange.start = event.start;
    this.daterange.end = event.end;
    this.daterange.label = event.label;
  }
  filterTypeChange(value) {
    this.reportType = value;
  }
  generateReport() {
    this.daterange.start = (`${this.fromSelectedYear}/${this.fromSelectedMonthVal}/01`);
    this.daterange.end = (`${this.toSelectedYear}/${this.toSelectedMonthVal}/31`);
    // console.log(this.daterange);

    let commissionData = {};
    if (this.daterange.start && this.daterange.end && this.reportType) {
      commissionData = {
        startDate: this.daterange.start,
        endDate: this.daterange.end,
        filterType: this.reportType,
        skip: 0,
        limit: 100000
      }
    } else {
      commissionData = {
        startDate: moment().format("YYYY/MM/DD"),
        endDate: moment().format("YYYY/MM/DD"),
        filterType: this.reportType,
        skip: 0,
        limit: 100000
      }
    }

    this.httpService.post(base_url + 'reports/count/stock', commissionData).subscribe((res: any) => {

      this.totlength = Math.ceil(res.data / 100000);
      console.log("this.totlength-=-=-=")
      console.log(this.totlength)
      this.recurse(0);
      // this.httpService.post(base_url + 'reports/getCommisionReport', commissionData).subscribe((res: any) => {
      //   this.commissionTable = res.data;
      //   this.toastr.success(res.message);
      // })
    })
  }

  recurse(skip) {
    this.daterange.start = (`${this.fromSelectedYear}/${this.fromSelectedMonthVal}/01`);
    this.daterange.end = (`${this.toSelectedYear}/${this.toSelectedMonthVal}/31`);

    var commissionData = {
      startDate: '',
      endDate: '',
      filterType: '',
      skip: skip,
      limit: 0,
      callcount: 0,
      totalcall: this.totlength
    };
    if (this.daterange.start && this.daterange.end && this.reportType) {
      commissionData = {
        startDate: this.daterange.start,
        endDate: this.daterange.end,
        filterType: this.reportType,
        skip: skip,
        limit: 100000,
        callcount: this.countofreport,
        totalcall: this.totlength
      }
    } else {
      commissionData = {
        startDate: moment().format("YYYY/MM/DD"),
        endDate: moment().format("YYYY/MM/DD"),
        filterType: this.reportType,
        skip: skip,
        limit: 100000,
        callcount: this.countofreport,
        totalcall: this.totlength
      }
    }

    this.httpService.post(base_url + 'reports/getCommisionReport', commissionData).subscribe((res: any) => {
      this.commissionTable = this.commissionTable + ' ' + res.data;
      console.log('res.data;', res.data);
      this.countofreport += 1;
      if (this.countofreport < this.totlength) {
        skip += 100000;
        this.recurse(skip);
      }
      if (this.countofreport == this.totlength)
        this.toastr.success(res.message);
    })
  }

  onFileBrowser(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('reportFile') as HTMLElement;
    element.click();
  }

  yourMethod() {
    this.confirmBox.show();
  }

  deleteAllData(showConfirm: boolean) {
    if (showConfirm) {
      this.httpService.post(base_url + 'reports/deleteAll', {}).subscribe((res: any) => {
        if (res.code == 200) {
          this.toastr.success(res.message);
        } else {
          this.toastr.error(res.message);
        }
      });
    }
  }

  saveFileData() {
    this.daterange.start = (`${this.selectedYear}/${this.selectedMonthVal}/01`);
    this.daterange.end = (`${this.selectedYear}/${this.selectedMonthVal}/31`);
    console.log(this.daterange.start);
    let setImpdate = (this.selectedYear + '/' + this.selectedMonthVal + "/01");
    //setImpdate =new Date(setImpdate).toLocaleDateString();
    console.log(setImpdate);

    this.formData = new FormData();
    this.formData.append('uploadFile', this.file);
    this.formData.append('reportType', this.reportType);
    this.formData.append('startDate', this.daterange.start);
    this.formData.append('endDate', this.daterange.end);
    this.formData.append('impDate', setImpdate)

    this.httpService.put(base_url + 'reports/uploadCommission', this.formData).subscribe((res: any) => {

      if (res.code == 200) {
        this.toastr.success(res.message);
      } else {
        this.toastr.error(res.message);
      }
    });
  }

  convertFile(event) {
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    if (true) {
      this.file = target.files[0];
    }
  }

  generatePDF() {

    var doc = new jsPDF('l', 'pt', "a4");
    var table = document.getElementsByClassName('commission-table')[0]


    doc.autoTable({
      html: table, theme: 'striped', styles: {
        overflow: 'linebreak',
        lineWidth: 0.02,
        lineColor: [217, 216, 216]
      }
    });
    doc.save(this.reportType + 'wise_commision_report.pdf');
  }
}
