import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { RegexService } from '../../services/regex.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  @ViewChild('form', { static: false }) loginForm: NgForm;
  emailPattern: RegExp;
  loginData = { 'email': '', 'password': '' };
  isSubmitted = false;
  passType = 'password';

  constructor(public router: Router, private httpService: HttpClient, private auth: AuthService, private regexService: RegexService, private toastr: ToastrService) {
    this.emailPattern = regexService.pattern.emailPattern;
  }

  ngOnInit() {}

  showPass() {
    if (this.passType == 'password') {
      this.passType = 'text'
    } else {
      this.passType = 'password'
    }
  }

  forgotPassword() {
    this.router.navigate(['forgot-password']);
  }

  onLoggedin(loginForm) {
    if (loginForm.form.status == 'INVALID') {
      Object.keys(loginForm.controls).forEach(key => {
        loginForm.controls[key].markAsDirty();
      });
      return false;
    }

    this.auth.checkLogin(this.loginData).subscribe((success: any) => {
        loginForm.reset();
        localStorage.setItem('authToken', success.data.token);
        localStorage.setItem('uid', success.data.user._id);
        this.toastr.success(success.message, '');

        if (this.auth.checkUserRole()) {
          //  admin-users
          this.router.navigate(['welcome-page']);
        } else {
          //users-agent
          this.router.navigate(['welcome-page']);
        }

      },
      err => {
        this.toastr.error(err.error.message, '');
      });
  }

}
