import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DispatchComponent } from './dispatch.component';

const routes: Routes = [{
  path: '',
  component: DispatchComponent,
  data: {
    title: 'Dispatch'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DispatchRoutingModule {}
