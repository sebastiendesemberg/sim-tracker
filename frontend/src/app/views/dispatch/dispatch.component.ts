import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';

import { environment } from '../../../environments/environment';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/ngx-bootstrap-pagination';
const base_url = environment.apiUrl;
@Component({
  selector: 'app-dispatch',
  templateUrl: './dispatch.component.html',
})

export class DispatchComponent implements OnInit {
  searchData: any;
  searchField: any;
  fillData: boolean = false;
  @ViewChild('inputFile') myInputVariable: ElementRef;
  private data: Array<any>;
  public page: number = 1;
  public itemsPerPage: number = 100;
  public maxSize: number = 5;
  public numPages: number = 5;
  public length: number = 0;
  contentArray = new Array(this.length).fill('');
  returnedArray: Array<any> = [];
  newreturnedArray: Array<any> = [];
  public rows: Array<any> = [];
  public rows1: Array<any> = [];
  openDropDown: boolean = false;
  filterData = { searchBy: { role: 'Supervisor' }, orderBy: { name } }
  supervisorData = [];
  dropdownvalue: any;
  boxId = [];
  dispatchData = {
    area: '',
    boxId: null,
    supervisor: '',
    supervisorId: ''
  };
  public columns: Array<any> = [
    {
      title: 'Box Barcode',
      name: '_id',
      sort: 'asc',
      // filtering: { filterString: '', placeholder: 'Search Barcode' }
    },

    // {
    //   title: 'Supervisor',
    //   name: 'supervisor',
    // },
  ];

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    className: ['table-striped', 'table-bordered', "table-sm", "table"]
  };

  public keysOfJson = ["kit", "msisdn", "barcode", "kit sim", "kit box", "area", "supervisor", "importedDate"];

  constructor(private httpService: HttpClient,
    private toastr: ToastrService, public auth: AuthService) {
    this.getSupervisorList();
    // this.onChangeTable(this.config);

  }

  ngOnInit(): void {
    var that = this;
    let UPC = '';
    document.getElementById('search').addEventListener('textInput', function (e) {
      if (e['data'].length >= 10) {
        console.log('IR scan textInput', e['data']);
        that.onBoxSearch([e['data']], '');
        e.preventDefault();
      }
    });
    document.getElementById('search').addEventListener('keypress', function (e) {
      that.returnedArray = [];
      const textInput = e.key || String.fromCharCode(e.keyCode);
      console.log("Text input", textInput);

      const targetName = e.target['localName'];
      let newUPC = '';
      if (textInput && textInput.length === 1 && targetName == 'input') {
        newUPC = UPC + textInput;
        if (newUPC.length >= 10) {
          console.log('barcode scanned:  ', newUPC);
          that.onBoxSearch(newUPC, '');
        }
      }
    });
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedArray = this.contentArray.slice(startItem, endItem);
  }


  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    var sortBy = '';
    var orderBy = {};
    var searchBy = {};
    searchBy = { supervisor: { $exists: true, $eq: "" } };

    for (let item of config.sorting.columns) {
      orderBy = { '_id': 1 }
      let sort;
      if (item.sort && item.sort.length > 0) {
        if (item.sort == 'asc') {
          sort = 1
        }
        else {
          sort = -1
        }
        sortBy = item.name
        orderBy = { [sortBy]: sort };
      }


      if (this.searchData && this.searchData.length > 0) {
        searchBy = { 'supervisor': { $exists: true, $eq: "" }, 'kit sim': this.searchData };
      }

    }

    let pageData = page.itemsPerPage * (page.page - 1);
    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'stock/create_list', filterData).subscribe((res: any) => {
      if (res.code == 200) {
        this.searchField = "";
        this.searchData = "";
        for (const key in res['data']) {
          if (res['data'].hasOwnProperty(key)) {
            const element = res['data'][key];
            console.log("Returend array", this.returnedArray);

            let existCheck = this.returnedArray.filter(x => x._id == element._id);
            if (existCheck && existCheck.length == 0) {
              this.returnedArray.push(element);
              if (this.boxId.indexOf(element._id) < 0) {
                this.boxId.push(element._id);
                console.log(this.boxId, "Push");
                this.dispatchData.boxId = this.boxId;
              }
            }
          }
        }
        //this.returnedArray=res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.returnedArray // this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error('Something went wrong');
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }


  public onChangeTable1(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    var sortBy = '';
    var orderBy = {};
    var searchBy = {};
    searchBy = { supervisor: { $exists: true, $eq: "" } };

    for (let item of config.sorting.columns) {
      orderBy = { '_id': 1 }
      let sort;
      if (item.sort && item.sort.length > 0) {
        if (item.sort == 'asc') {
          sort = 1
        }
        else {
          sort = -1
        }
        sortBy = item.name
        orderBy = { [sortBy]: sort };
      }

      if (this.searchField && this.searchField.length > 0) {
        searchBy = {
          'supervisor': { $exists: true, $eq: "" },
          // $or: [{ 'kit sim': { '$regex': '.*' + this.searchField + '.*' } }, { 'kit box': { '$regex': '.*' + this.searchField + '.*' } }]
          'kit sim': { '$regex': '.*' + this.searchField + '.*' }
        };
      }
    }

    let pageData = page.itemsPerPage * (page.page - 1);
    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'stock/create_list', filterData).subscribe((res: any) => {
      if (res.code == 200) {
        for (const key in res['data']) {
          if (res['data'].hasOwnProperty(key)) {
            const element = res['data'][key];

            let existCheck = this.newreturnedArray.filter(x => x._id == element._id);
            if (existCheck && existCheck.length == 0) {
              this.newreturnedArray.push(element);
              this.searchField = "";
              this.searchData = "";
              // if (this.boxId.indexOf(element._id) < 0) {
              //   this.boxId.push(element._id);
              //   console.log(this.boxId, "Push");
              //   this.dispatchData.boxId = this.boxId;
              // }
            }
          }
        }
        //this.returnedArray=res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.newreturnedArray // this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error('Something went wrong');
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }
  onPaste(event) {
    console.log(event);
  }
  removeBox(removeBoxId: any) {
    let indexOfBox = this.returnedArray.findIndex(x => x._id == removeBoxId);
    this.returnedArray.splice(indexOfBox, 1);

    const index = this.boxId.indexOf(removeBoxId) // Find the index of stored id
    index > -1 ? this.boxId.splice(index, 1) : '';
    this.dispatchData.boxId = this.boxId;

  }
  onCellClick(event) {
    console.log("Event", event);
  }
  getSupervisorList() {
    this.httpService.post(base_url + 'common/User', this.filterData).subscribe((res: any) => {
      this.supervisorData = res['data'];
      let filteredData = this.supervisorData

      this.rows1 = filteredData;
    })
  }

  selectObj(event) {
    this.dropdownvalue = event.target.value;
    let filter = this.supervisorData.filter(x => x.name == event.target.value);
    console.log(filter, "filter");
    this.dispatchData.area = filter[0].area;
    this.dispatchData.supervisor = filter[0].name;
    this.dispatchData.supervisorId = filter[0]._id;
  }
  onSelectCheckData(event) {
    event = event.event;
    if (this.boxId.indexOf(event) < 0) {
      this.boxId.push(event);
      this.dispatchData.boxId = this.boxId;
    }
    else {
      const index = this.boxId.indexOf(event) // Find the index of stored id
      index > -1 ? this.boxId.splice(index, 1) : '';
      this.dispatchData.boxId = this.boxId;
    }
  }
  onBoxSearch(value, field) {
    if (field === 'add') {
      this.searchData = value;
      let getIndex = this.newreturnedArray.findIndex(x => x._id == value);
      getIndex > -1 ? this.newreturnedArray.splice(getIndex, 1) : '';
      this.onChangeTable(this.config);
    }
    else {
      this.onChangeTable(this.config);
    }


  }
  onSearch(value) {
    this.onChangeTable1(this.config);

  }
  onDispatchData() {
    const thatData = this;
    console.log(this.dispatchData, 'Dataa to update');
    if (this.dispatchData.supervisor === '' || this.dispatchData.boxId === null || this.dispatchData.boxId.length === 0) {
      this.toastr.error('Please select supervisor');
      return false;
    }

    this.httpService.put(base_url + 'stock/dispatch_update', this.dispatchData).subscribe((res: any) => {
      console.log('Dispatch updated', res);
      if (res.code === 200) {
        thatData['dispatchData'] = {
          area: thatData['dispatchData'].area,
          boxId: null,
          supervisor: thatData.dispatchData.supervisor,
          supervisorId: thatData.dispatchData.supervisorId
        };
        this.returnedArray = [];
        this.boxId = [];

        // console.log('Diiiii' , thatData.dispatchData);
        // thatData.dispatchData.supervisor = '';
        // thatData.dispatchData.boxId = null;
        this.onChangeTable(null, this.config);
        this.toastr.success(res.message);
      }

    });
  }
}
