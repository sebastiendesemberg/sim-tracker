import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from './ng-table-module';
import { DispatchComponent } from './dispatch.component';
import { DispatchRoutingModule } from './dispatch-routing.module';

@NgModule({
  declarations: [DispatchComponent],
  imports: [
    DispatchRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
    Ng2TableModule,  
    CommonModule
  ]
})
export class DispatchModule {}
