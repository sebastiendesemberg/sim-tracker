import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { RegexService } from '../../services/regex.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './forgotpassword.component.html'
  // styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  @ViewChild('form', { static: false }) recoverPass: NgForm;
  emailPattern: RegExp;
  receoverpassData = { 'email': '' };
  sendpassMail = false;
  userData: any;

  constructor(public router: Router, private auth: AuthService, private regexService: RegexService, private toastr: ToastrService, ) {
    this.emailPattern = regexService.pattern.emailPattern;
  }

  ngOnInit(): void {}


  recoverPassword(recoverPass) {

    if (recoverPass.form.status == 'INVALID') {
      Object.keys(recoverPass.controls).forEach(key => {
        recoverPass.controls[key].markAsDirty();
      });
      return false;
    }

    this.sendpassMail = true;
    this.auth.recoverPass(this.receoverpassData).subscribe(res => {
      this.userData = res;
      this.toastr.success(this.userData.message, '');
      recoverPass.reset();
      this.router.navigate(['login']);
      this.sendpassMail = false;
    }, err => {
      this.sendpassMail = false;
      this.toastr.error(err.error.message, '');

    });;
  }

}
