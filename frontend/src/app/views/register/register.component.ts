import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import * as crypto from 'crypto-js';
import { AuthService } from '../../services/auth.service';
import { RegexService } from '../../services/regex.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {

  @ViewChild('form', { static: false }) signForm: NgForm;
  emailPattern: RegExp;
  passwordPattern: RegExp;
  signupData = { 'email': '', 'password': '' };
  sentmail = false;
  mailexist = false;
  showTool = false;
  checkUpper = false;
  checkLower = false;
  checkNumber = false;
  checkSpecial = false;
  checksigup = false;
  passType = 'password';
  signUpData: any;

  constructor(private httpService: HttpClient, private auth: AuthService, private regexService: RegexService, private toastr: ToastrService, ) {
    this.emailPattern = regexService.pattern.emailPattern;
    this.passwordPattern = regexService.pattern.passwordPattern;
  }

  ngOnInit() {}

  showPass() {
    if (this.passType == 'password') {
      this.passType = 'text'
    } else {
      this.passType = 'password'
    }
  }

  changeToolColor(passString) {
    var lowerCaseLetters = /[a-z]/g;
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;
    var special = /[$@$#!%*?&]/g;
    if (passString && passString.match(upperCaseLetters)) {
      this.checkUpper = true;
    } else {
      this.checkUpper = false;
    }
    if (passString && passString.match(lowerCaseLetters)) {
      this.checkLower = true;
    } else {
      this.checkLower = false;
    }
    if (passString && passString.match(numbers)) {
      this.checkNumber = true;
    } else {
      this.checkNumber = false;
    }
    if (passString && passString.match(special)) {
      this.checkSpecial = true;
    } else {
      this.checkSpecial = false;
    }

  }
  ToolShow(status) {
    this.showTool = status;
  }

  onSignup(signForm) {
    if (signForm.form.status == 'INVALID') {
      Object.keys(signForm.controls).forEach(key => {
        signForm.controls[key].markAsDirty();
      });
      return;
    }
    this.passType = 'password'
    this.checksigup = true;
    this.auth.userSignup(this.signupData).subscribe(res => {
      this.signUpData = res;
      this.sentmail = true;
      this.signupData.password = ''
      signForm.reset();
      this.toastr.success(this.signUpData.message, '');
      this.checksigup = false;

    }, err => {
      this.signupData.password = ''
      this.sentmail = false;

      this.toastr.error(err, '');
      this.checksigup = false;

    });
  }

}
