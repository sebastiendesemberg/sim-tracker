import { Component, OnInit, ViewChild, NgZone } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import * as crypto from 'crypto-js';
import { AuthService } from "../../services/auth.service";
import { RegexService } from "../../services/regex.service";
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { NgxConfirmBoxService } from 'ngx-confirm-box';
const base_url = environment.apiUrl;

@Component({
  templateUrl: 'users.component.html'
})

export class UsersComponent implements OnInit {

  @ViewChild('form', { static: false }) userForm: NgForm;
  emailPattern: RegExp;
  phonePattern: RegExp;
  passwordPattern: RegExp;
  userList: any;
  areaList: any;
  // editData = false;
  id: any;
  reppass: any;
  user = {
    name: '',
    surName: '',
    cell: '',
    _id: '',
    area: '',
    email: '',
    password: '',
    conpassword: '',
    active: true,
    role: '',
  };
  maxLength: number;
  minLength: number;
  passwordMinLength: number;
  passwordMaxLength: number;
  checkUpper: boolean = false;
  checkLower: boolean = false;
  checkNumber: boolean = false;
  checkSpecial: boolean = false;
  showTool: boolean = false;
  deleteId:any;
  bgColor = 'rgba(0,0,0,0.5)'; // overlay background color
  confirmHeading = '';
  confirmContent = "Are you sure, want to delete this?";
  confirmCanceltext = "Cancel";
  confirmOkaytext = "Okay";



  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
    private confirmBox: NgxConfirmBoxService,
    private httpService: HttpClient,
    private regexService: RegexService,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.emailPattern = regexService.pattern.emailPattern;
    this.phonePattern = regexService.pattern.phonePattern;
    this.passwordPattern = regexService.pattern.passwordPattern;
    this.maxLength = 14;
    this.minLength = 12;
    this.passwordMinLength = 8;
    this.passwordMaxLength = 32;
    this.GetUserList();
    this.GetAreaList();

  }

  ngOnInit() { }

  editDatashow(data) {
    // console.log("areaList", this.areaList)
    if (this.areaList && this.areaList.length <= 0) {
      this.toastr.warning('Please Add Area', '');
    }
    this.auth.editData = true;
    this.user = data;
  }
  keyPress(event: any) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43) {
      return false;
    }
    return true;
  }

  GetUserList() {
    this.httpService.get(base_url + 'common/User').subscribe(res => {
      this.userList = res['data'];
    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }

  GetAreaList() {
    this.httpService.get(base_url + 'common/Area').subscribe(res => {

      this.areaList = res['data'];

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }

  adduser() {
    // console.log("areaList", this.areaList)
    if (this.areaList && this.areaList.length <= 0) {
      this.toastr.warning('Please Add Area', '');
    }
    this.auth.editData = true;
    console.log("Edit data",this.auth.editData);

    this.location.replaceState('/users/add');
  }

  resetData(userForm) {
    this.user._id='';
    this.user.password='';
    this.user.conpassword='';
    this.auth.editData = false;

    userForm.reset();
    this.GetUserList();
    this.location.replaceState('/users');

  }

  yourMethod(id) {
    this.deleteId = id;
    this.confirmBox.show();
  }

  deleteData(showConfirm: boolean) {
    if (showConfirm) {
      this.httpService.delete(base_url + 'common/User/' + this.deleteId).subscribe(res => {
        this.GetUserList();
        this.toastr.success('User Deleted', '');
      }, err => {
        this.GetUserList()
        this.toastr.error(err.error.message, '');
      });
    }
  }

  submit(userForm) {
    // console.log(userForm.form.status)
    if (userForm.form.status == "INVALID") {
      Object.keys(userForm.controls).forEach(key => {
        userForm.controls[key].markAsDirty();
      });
      return false;
    }

    if (this.user._id) {
      this.httpService.post(base_url + 'common/User/' + this.user._id, this.user).subscribe((res1: any) => {
        this.toastr.success('User Updated');
        this.user._id='';
        this.user.password='';
        this.user.conpassword='';
        userForm.reset();
        this.GetUserList();
        this.auth.editData = false;
      }, err => {
        this.auth.editData = true;
        this.toastr.error(err.error.message, '');
      });
    } else {
      delete this.user._id;
      this.user.password = crypto.AES.encrypt(this.user.password, environment.encryptionKey).toString();
      this.httpService.put(base_url + 'common/User', this.user).subscribe((res1: any) => {
        this.toastr.success('User Added');
        this.user._id='';
        this.user.password='';
        this.user.conpassword='';
        userForm.reset();
        this.GetUserList();
        this.auth.editData = false;
        this.location.replaceState('/users');
      }, err => {
        this.auth.editData = true;
        // this.toastr.error(err.error.message, '');
        this.toastr.error("Email Already Exists");
      });

    }

  }
}
