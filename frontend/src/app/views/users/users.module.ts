import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users-routing.module';
import { NgxConfirmBoxModule, NgxConfirmBoxService } from 'ngx-confirm-box';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    UsersRoutingModule,
	NgxConfirmBoxModule,
    FormsModule,
    CommonModule
	],
	providers: [
		NgxConfirmBoxService
	]
})
export class UsersModule {}