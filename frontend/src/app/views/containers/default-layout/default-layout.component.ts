import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { navItems, navAdminItems } from '../../../_nav';
import { AuthService } from '../../../services/auth.service';
import { User } from '../../../models/user';
import { Location } from '@angular/common';
import { filter, pairwise } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems;
  user: User;
  navigateUrl;
  currentPath: string;
  previousUrl: string;
  previousPath: any;

  toggleMinimize(e) {
    console.log("Toggle", e);

    this.sidebarMinimized = e;
  }

  constructor(public router: Router, public auth: AuthService, public location: Location) {
    this.user = this.auth.currentUserValue;
    if (this.auth.checkUserRole()) {
      this.navItems = navAdminItems;
    } else {
      this.navItems = navItems;
    }
  }

  ngOnInit() {
    let currentRoute = this.router.url.split('/');
    this.currentPath = this.toTitleCase(currentRoute[1]);
    if (this.currentPath.indexOf("?")) {
      let tempUrl = this.currentPath.split('?');
      this.currentPath = tempUrl[0];
    }


    if (this.currentPath == 'Brick'){
      this.previousPath="Stock";
    }
    else if (this.currentPath == 'Pack') {
      this.previousPath="Brick";
    } else{
      this.previousPath=this.currentPath;
    }

  }
  goBack() {
    if (this.currentPath == 'Brick' || this.currentPath == 'Pack') {
      this.location.back();
    }
  }
  toTitleCase(str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  }
  onLoggedout() {
    this.auth.logOut();
  }
  onClick() {
    this.auth.editData = false;
    // console.log("eee",this.router.url);
    if (this.router.url == '/users') {
      this.location.replaceState('/users');
    }
    else if (this.router.url == '/area') {
      this.location.replaceState('/area');

    }
    else if (this.router.url == '/agent') {
      this.location.replaceState('/agent');

    }

    // this.router.navigate([this.navigateUrl]);

  }
}
