import { Component, OnInit, ViewChild, NgZone } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from "../../services/auth.service";
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';

const base_url = environment.apiUrl;

@Component({
  templateUrl: './area.component.html'
})
export class AreaComponent implements OnInit {

  @ViewChild('form', { static: false }) areaForm: NgForm;
  areaList: any;
  editData = false;
  area = {
    name: '',
    // value: '',
    _id: '',
    userCounts: []
  };

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
    private httpService: HttpClient,
    private toastr: ToastrService,
    private location: Location
  ) {
    this.GetList();
  }

  ngOnInit() {}

  editDatashow(data) {
    this.auth.editData = true;
    this.area = data;
  }

  GetList() {
    this.httpService.get(base_url + 'common/Area').subscribe(res => {
      this.areaList = res['data'];
    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }

  addData() {
    this.auth.editData = true;
    this.location.replaceState('/area/add');
  }

  resetData(areaForm) {
    this.auth.editData = false;
    areaForm.reset();
    this.GetList();
    this.location.replaceState('/area');

  }

  deleteData(id) {
    this.httpService.delete(base_url + 'common/Area/' + id).subscribe(res => {
      this.GetList();
      this.toastr.success('Area Deleted', '');
    }, err => {
      this.GetList()
      this.toastr.error(err.error.message, '');
    });

  }

  submit(areaForm) {
    if (areaForm.form.status == "INVALID") {
      Object.keys(areaForm.controls).forEach(key => {
        areaForm.controls[key].markAsDirty();
      });
      return false;
    }

    if (this.area._id) {

      this.httpService.post(base_url + 'common/Area/' + this.area._id, this.area).subscribe((res1: any) => {
        areaForm.reset();
        this.GetList();
        this.toastr.success('Area Updated');
        this.auth.editData = false;

      }, err => {
        this.auth.editData = true;
        this.toastr.error(err.error.message, '');
      });

    } else {

      delete this.area._id;
      this.httpService.put(base_url + 'common/Area', this.area).subscribe((res1: any) => {
        areaForm.reset();
        this.GetList();
        this.toastr.success('Area Added');
        this.auth.editData = false;
      }, err => {
        this.auth.editData = true;
        this.toastr.error(err.error.message, '');
      });

    }

  }

}
