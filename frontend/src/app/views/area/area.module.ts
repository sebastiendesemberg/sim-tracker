import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AreaComponent } from './area.component';
import { AreaRoutingModule } from './area-routing.module';

@NgModule({
  declarations: [AreaComponent],
  imports: [
    AreaRoutingModule,
    FormsModule,
    CommonModule
  ]
})
export class AreaModule {}
