import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
const base_url = environment.apiUrl;

@Component({
  selector: 'app-agent-box',
  templateUrl: './agent-box.component.html',
  styleUrls: ['./agent-box.component.css']
})
export class AgentBoxComponent implements OnInit {
  agentId: any;
  supervisorId: any;
  boxList: Array < any > = [];
  boxDataList: Array < any > = [];
  public rows: Array < any > = [];
  agentData: any;
  searchData: any;
  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 5;
  public length: number = 0;
  boxId = [];
  searchBy = '';
  public columns: Array < any > = [{
      title: 'Box Barcode',
      name: '_id',
      sort: 'asc',
      // filtering: { filterString: '', placeholder: 'Search Barcode' }
    },

    // {
    //   title: 'Supervisor',
    //   name: 'supervisor',
    // },
  ];
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    className: ['table-striped', 'table-bordered', "table-sm", "table"]
  };
  constructor(private route: ActivatedRoute, private httpService: HttpClient,
    private toastr: ToastrService, public auth: AuthService, private router: Router) {
    this.agentId = this.route.snapshot.queryParamMap.get('agentId');
    this.supervisorId = this.route.snapshot.queryParamMap.get('supervisorId');
    this.httpService.get(base_url + 'agent/getAgentSelectedList/' + this.agentId).subscribe((response: any) => {
      // console.log(response, "sasad");
      if (response.code == 200) {
        this.agentData = response.data;
        //     this.httpService.get(base_url + 'agent/getAgentBoxList/' + this.agentId + '/' + this.supervisorId).subscribe((res: any) => {
        //       console.log("Reeeeee",res);

        //       if (res.code == 200) {
        //         if (response.data.assignedBoxes.length > 0) {
        //           this.boxDataList = response.data.assignedBoxes;
        //           response.data.assignedBoxes.forEach(element => {
        //             this.boxList.push({ "_id": element, "selected": true });
        //           });
        //         }
        //         res.data.forEach(element => {
        //           this.boxList.push({ "_id": element, "selected": false });
        //         })
        //       } else {
        //         this.toastr.error(res.message);
        //       }
        //     })
      } else {
        this.toastr.error(response.message);
      }
    })

  }

  ngOnInit(): void {}

  onSelectCheckData(boxId) {
    console.log("Check box value", boxId);
    if (this.boxDataList.indexOf(boxId) < 0) {
      this.boxDataList.push(boxId);
      // console.log(this.boxDataList, "Push");
      //this.dispatchData.boxId = this.boxDataList;
    } else {
      const index = this.boxDataList.indexOf(boxId) // Find the index of stored id
      index > -1 ? this.boxDataList.splice(index, 1) : '';
      // console.log(this.boxDataList, 'Pop');
      //this.dispatchData.boxId = this.boxDataList;
    }

  }
  backToPage() {
    this.router.navigate(['agent']);
  }
  onBoxSearch(value) {
    this.searchBy = value;
    this.onChangeTable(this.config);
    // searchBy = { supervisor: { $exists: true, $eq: "" } };
    // if (this.searchData && this.searchData.length > 0) {
    //   searchBy = { 'supervisor': { $exists: true, $eq: "" }, 'kit sim': this.searchData };
    // }

    // this.httpService.get(base_url + 'agent/getAgentSelectedList/' + this.agentId + '/' + this.supervisorId).subscribe((response: any) => {
    //   if (response.code == 200) {
    //     this.agentData = response.data;
    //     console.log("Ressss",response);

    //     this.httpService.get(base_url + 'agent/getAgentBoxList/' + this.agentId + '/' + this.supervisorId,{params:{search:this.searchBy}}).subscribe((res: any) => {
    //       console.log("Reeeeee",res);

    //       if (res.code == 200) {
    //         if (response.data.assignedBoxes.length > 0) {
    //           this.boxDataList = response.data.assignedBoxes;
    //           response.data.assignedBoxes.forEach(element => {
    //           let existCheck=this.boxList.filter(x=>x._id==element);
    //           if (existCheck && existCheck.length == 0) {
    //             this.boxList.push({ "_id": element, "selected": true });
    //           }
    //           });
    //         }
    //         res.data.forEach(element => {
    //           let existCheck=this.boxList.filter(x=>x._id==element);
    //           console.log("Check list",existCheck);
    //           if (existCheck && existCheck.length == 0) {

    //             this.boxList.push({ "_id": element, "selected": false });
    //           }
    //         })
    //       } else {
    //         this.toastr.error(res.message);
    //       }
    //     })
    //   } else {
    //     this.toastr.error(response.message);
    //   }
    // })

    // this.httpService.get(base_url + 'agent/getAgentBoxList/' + this.agentId + '/' + this.supervisorId,{params:{search:this.searchBy}}).subscribe((response: any) => {
    //   if (response.code == 200) {
    //     console.log("Response of search",response);
    //       if (response.code == 200) {
    //         if (response.data.assignedBoxes.length > 0) {
    //           this.boxDataList = response.data.assignedBoxes;
    //           response.data.assignedBoxes.forEach(element => {
    //             this.boxList.push({ "_id": element, "selected": true });
    //           });
    //         }
    //         response.data.forEach(element => {
    //           this.boxList.push({ "_id": element, "selected": false });
    //         })
    //       } else {
    //         this.toastr.error(response.message);
    //       }

    // if(res.data.length>0){
    //   res.data.forEach(element => {
    //     let existCheck=this.boxList.filter(x=>x._id==element);
    //     console.log("EEEEE",existCheck);
    //     if(existCheck && existCheck.length>0){
    //       let newList=[];
    //      newList.push(existCheck);
    //      this.boxList=newList;
    //       console.log("BoxList",this.boxList);

    //     }

    //   });
    // }
    //         for (const key in res['data']) {
    //           if (res['data'].hasOwnProperty(key)) {
    //             const element = res['data'][key];
    // console.log(element,"Element");

    //             let existCheck = this.boxList.filter(x => x._id == element._id);
    //             if (existCheck && existCheck.length == 0) {
    //               this.boxList.push(element);
    //               if (this.boxId.indexOf(element._id) < 0) {
    //                 this.boxId.push(element._id);
    //                 console.log(this.boxId, "Push");
    //                 // this.dispatchData.boxId = this.boxId;
    //               }
    //             }
    //           }
    //         }
    // } else {
    //   this.toastr.error('Something went wrong');
    // }

    // }, err => {
    //   this.toastr.error(err.error.message, '');
    // });
  }
  onAssignData() {

    if (this.agentData && this.agentData.assignedBoxes) {
      this.agentData.assignedBoxes.forEach(element => {
        if (this.boxId.indexOf(element) < 0) {
          this.boxId.push(element);
        }
      });
    }
    console.log(this.boxList, "===", this.boxId);
    // return false;
    let agentData = {
      agentId: this.agentId,
      supervisorId: this.supervisorId,
      assignedBoxes: this.boxId
    }
    this.httpService.put(base_url + 'agent/assignBoxToAgent/', agentData).subscribe((res: any) => {
      if (res.code == 200) {
        this.boxId = [];
        this.boxList = [];
        this.toastr.success(res.message);
      } else {
        this.toastr.error(res.message);
      }
    })
  }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    var sortBy = '';
    var orderBy = {};
    var searchBy = {};


    for (let item of config.sorting.columns) {
      orderBy = { '_id': 1 }
      let sort;
      if (item.sort && item.sort.length > 0) {
        if (item.sort == 'asc') {
          sort = 1
        } else {
          sort = -1
        }
        sortBy = item.name
        orderBy = {
          [sortBy]: sort
        };
      }

      if (this.searchData && this.searchData.length > 0) {
        searchBy = this.searchData;
      }
    }

    let pageData = page.itemsPerPage * (page.page - 1);
    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'agent/getAgentBoxList/' + this.agentId, filterData).subscribe((res: any) => {
      console.log("agent boxex-->", res);

      if (res.code == 200) {
        for (const key in res['data']) {
          if (res['data'].hasOwnProperty(key)) {
            const element = res['data'][key];
            console.log("element-======-=-=-=-=--==-");
            console.log(element);

            let existCheck = this.boxList.filter(x => x == element);
            if (existCheck && existCheck.length == 0) {
              this.boxList.push(element);
              // this.boxId.push(element);

              if (this.boxId.indexOf(element) < 0) {
                this.boxId.push(element);
                // console.log(this.boxId, "Push");
                //   // this.dispatchData.boxId = this.boxId;
              }
            }
          }
        }
        //this.boxList=res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.boxList // this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error('Something went wrong');
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }
  removeBox(removeBoxId: any) {
    let indexOfBox = this.boxList.findIndex(x => x == removeBoxId);
    this.boxList.splice(indexOfBox, 1);
    const index = this.boxId.findIndex(x => x == removeBoxId);
    this.boxId.splice(indexOfBox, 1);
    // const index = this.boxId.indexOf(removeBoxId) // Find the index of stored id
    // index > -1 ? this.boxId.splice(index, 1) : '';
    //this.dispatchData.boxId = this.boxId;

  }

}
