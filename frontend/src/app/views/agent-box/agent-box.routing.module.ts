import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentBoxComponent } from './agent-box.component';

const routes: Routes = [{
  path: '',
  component: AgentBoxComponent,
  data: {
    title: 'Agent-Box'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentBoxRoutingModule {}
