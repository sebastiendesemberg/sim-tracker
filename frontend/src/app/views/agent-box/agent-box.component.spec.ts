import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentBoxComponent } from './agent-box.component';

describe('AgentBoxComponent', () => {
  let component: AgentBoxComponent;
  let fixture: ComponentFixture<AgentBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
