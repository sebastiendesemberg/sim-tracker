import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgentBoxComponent } from './agent-box.component';
import { AgentBoxRoutingModule } from './agent-box.routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';



@NgModule({
  declarations: [AgentBoxComponent],
  imports: [
    AgentBoxRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
    CommonModule
  ]
})
export class AgentBoxModule {}
