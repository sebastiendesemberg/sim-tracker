import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WelcomePageComponent } from './welcome-page.component';
import { WelcomePageRoutingModule } from './welcome-page-routing.module';

@NgModule({
  declarations: [WelcomePageComponent],
  imports: [
    WelcomePageRoutingModule,
    FormsModule,
    CommonModule
  ]
})
export class WelcomePageModule {}
