import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from './ng-table-module';
import { PackComponent } from './pack.component';
import { PackRoutingModule } from './pack-routing.module';

@NgModule({
  declarations: [PackComponent],
  imports: [
    PackRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
    Ng2TableModule,
    CommonModule
  ]
})
export class PackModule {}
