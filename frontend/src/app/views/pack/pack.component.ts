import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '../../../../node_modules/@angular/router';
import { HttpClient } from '../../../../node_modules/@angular/common/http';
import { ToastrService } from '../../../../node_modules/ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { environment } from '../../../environments/environment';
import * as moment from 'moment';

const base_url = environment.apiUrl;

@Component({
  selector: 'app-pack',
  templateUrl: './pack.component.html',
  styleUrls: ['./pack.component.css']
})
export class PackComponent implements OnInit {

  brickId: any;
  boxId: any;
  searchData: any;

  @ViewChild('inputFile') myInputVariable: ElementRef;
  private data: Array<any>;
  public page: number = 1;
  public itemsPerPage: number = 100;
  public maxSize: number = 5;
  public numPages: number = 5;
  public length: number = 0;
  contentArray = new Array(this.length).fill('');
  returnedArray: Array<any> = [];
  public rows: Array<any> = [];
  public columns: Array<any> = [
    {
      title: 'Pack',
      name: '_id',
      sort: 'asc',
      className: 'table-header-col1'
      // filtering: { filterString: '', placeholder: 'Search Brick' }
    },
    {
      title: 'Imported Date',
      name: 'importedDate',
      sort: 'asc',
      className: 'table-header-col1'
    },
    {
      title: 'Area',
      name: 'area',
      sort: 'asc',
      className: 'table-header-col2'
    },
    {
      title: 'Supervisor',
      name: 'supervisor',
      sort: 'asc',
    },
  ];

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    className: ['table-striped', 'table-bordered', "table-sm", "table"]
  };

  public keysOfJson = ["kit", "msisdn", "barcode", "kit sim", "kit box", "area", "supervisor", "importedDate"];

  constructor(private route: ActivatedRoute, private httpService: HttpClient,
    private toastr: ToastrService, public auth: AuthService, private router: Router) {
    this.brickId = this.route.snapshot.queryParamMap.get('brickId');
    this.boxId = this.route.snapshot.queryParamMap.get('boxId');
    console.log("brickId", this.brickId);

  }

  ngOnInit(): void {
    this.onChangeTable(this.config);
  }
  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    var sortBy = '';
    var orderBy = {};
    var searchBy = {};
    searchBy = { 'kit box': { '$regex': '.*' + this.brickId + '.*' } };

    for (let item of config.sorting.columns) {
      orderBy = { '_id': 1 }
      let sort;
      if (item.sort && item.sort.length > 0) {
        if (item.sort == 'asc') {
          sort = 1
        }
        else {
          sort = -1
        }
        sortBy = item.name
        orderBy = { [sortBy]: sort };
      }

      if (this.searchData && this.searchData.length > 0) {
        searchBy = { 'kit': { '$regex': '.*' + this.searchData + '.*', '$options': 'i' } };
      }
    }

    let pageData = page.itemsPerPage * (page.page - 1);

    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'stock/pack_list', filterData).subscribe((res: any) => {
      if (res.code == 200) {
        for (let val of res['data']) {
          let date = val.stocks[0].importedDate;
          val.importedDate = moment(date).format('DD-MM-YYYY');
          val.area = val.stocks[0].area;
          val.supervisor = val.stocks[0].supervisor;
        }
        this.returnedArray = res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.returnedArray //this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error("Something went wrong");
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }

  onCellClick(event) {
    // console.log("Event on cell",event);
    // this.router.navigate(['pack'],{queryParams:{brickId:event.row['kit box']}});

  }
  onBack() {
    // this.router.navigate(['bick'],{queryParams:{boxId:this.boxId}});
    window.history.back();

  }
  onBoxSearch(value) {
    this.onChangeTable(this.config);
  }

}
