import { Component, OnInit, ViewChild, NgZone } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
const base_url = environment.apiUrl;

@Component({
  templateUrl: '404.component.html'
})
export class P404Component implements OnInit {


  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private httpService: HttpClient
  ) {

    this.GetUserList();
  }


  ngOnInit() {}


  GetUserList() {
    this.httpService.get(base_url + 'common/User').subscribe((res: any) => {
      if (res.code == 200) {
        this.router.navigate(['login']);
      }
    });
  }

}
