import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrickComponent } from './brick.component';

const routes: Routes = [{
  path: '',
  component: BrickComponent,
  data: {
    title: 'Stock'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrickRoutingModule {}
