import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { Ng2TableModule } from './ng-table-module';
import { BrickComponent } from './brick.component';
import { BrickRoutingModule } from './brick-routing.module';

@NgModule({
  declarations: [BrickComponent],
  imports: [
    BrickRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
    Ng2TableModule,
    CommonModule
  ]
})
export class BrickModule {}
