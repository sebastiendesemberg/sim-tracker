import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { environment } from '../../../environments/environment';
const base_url = environment.apiUrl;
@Component({
  selector: 'app-view-boxes',
  templateUrl: './view-boxes.component.html',
  styleUrls: ['./view-boxes.component.css']
})
export class ViewBoxesComponent implements OnInit {
  agentId: string;
  agentData: any;
  public rows: Array < any > = [];
  searchData: any;
  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 5;
  public length: number = 0;
  boxDataList = [];
  searchBy = '';
  public columns: Array < any > = [{
    title: 'Box Barcode',
    name: '_id',
    sort: 'asc',
    // filtering: { filterString: '', placeholder: 'Search Barcode' }
  }];
  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    className: ['table-striped', 'table-bordered', "table-sm", "table"]
  };
  constructor(private route: ActivatedRoute, private httpService: HttpClient,
    private toastr: ToastrService, public auth: AuthService, private router: Router) {
    this.agentId = this.route.snapshot.queryParamMap.get('agentId');
    this.httpService.get(base_url + 'agent/getAgentSelectedList/' + this.agentId).subscribe((response: any) => {
      if (response.code == 200) {
        this.agentData = response.data.agent;
        this.boxDataList = response.data.dataarray;

      } else {
        this.toastr.error(response.message);
      }
    })
  }

  ngOnInit(): void {}
  backToPage() {
    this.router.navigate(['agent']);
  }
  onAgentSearch(value) {
    this.onChangeTable(this.config);
  }
  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    var sortBy = '';
    var orderBy = {};
    var searchBy = {};


    for (let item of config.sorting.columns) {
      orderBy = { '_id': 1 }
      let sort;
      if (item.sort && item.sort.length > 0) {
        if (item.sort == 'asc') {
          sort = 1
        } else {
          sort = -1
        }
        sortBy = item.name
        orderBy = {
          [sortBy]: sort
        };
      }

      if (this.searchData && this.searchData.length > 0) {
        searchBy = this.searchData;
      } else {
        searchBy = "";
      }
    }


    let pageData = page.itemsPerPage * (page.page - 1);
    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'agent/searchAgentBox/' + this.agentId, filterData).subscribe((res: any) => {

      if (res.code == 200) {
        this.boxDataList = [];
        for (const key in res['data']) {
          if (res['data'].hasOwnProperty(key)) {
            const element = res['data'][key];

            // this.boxDataList=element;
            //this.boxDataList=[];
            let existCheck = this.boxDataList.filter(x => x == element);
            // console.log(existCheck);

            if (existCheck && existCheck.length == 0) {

              this.boxDataList.push(element);
              if (this.boxDataList.indexOf(element) < 0) {

              }
              // this.boxId.push(element);


            }
          }
        }
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.boxDataList;

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error('Something went wrong');
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }
}
