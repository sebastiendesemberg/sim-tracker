import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBoxesComponent } from './view-boxes.component';

describe('ViewBoxesComponent', () => {
  let component: ViewBoxesComponent;
  let fixture: ComponentFixture<ViewBoxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBoxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBoxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
