import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewBoxesComponent } from './view-boxes.component';

const routes: Routes = [{
  path: '',
  component: ViewBoxesComponent,
  data: {
    title: 'Agent-Box'
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewBoxesRoutingModule {}
