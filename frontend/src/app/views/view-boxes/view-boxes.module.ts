import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewBoxesComponent } from './view-boxes.component';
import { ViewBoxesRoutingModule } from './view-boxes.routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';



@NgModule({
  declarations: [ViewBoxesComponent],
  imports: [
    ViewBoxesRoutingModule,
    FormsModule,
    PaginationModule.forRoot(),
    CommonModule
  ]
})
export class ViewBoxesModule {}
