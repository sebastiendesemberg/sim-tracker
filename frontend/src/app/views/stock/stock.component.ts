import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';

import { environment } from '../../../environments/environment';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/ngx-bootstrap-pagination';
import { Router } from '../../../../node_modules/@angular/router';
import * as moment from 'moment';
import { NgxConfirmBoxService } from 'ngx-confirm-box';
const base_url = environment.apiUrl;
@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})

export class StockComponent implements OnInit {
  searchData: any;
  @ViewChild('inputFile') myInputVariable: ElementRef;
  private data: Array<any>;
  public page: number = 1;
  public itemsPerPage: number = 100;
  public maxSize: number = 5;
  public numPages: number = 5;
  public length: number = 0;
  contentArray = new Array(this.length).fill('');
  returnedArray: Array<any> = [];
  public rows: Array<any> = [];
  user:any;
  bgColor = 'rgba(0,0,0,0.5)'; // overlay background color
  confirmHeading = '';
  confirmContent = "Are you sure, want to delete this?";
  confirmCanceltext = "Cancel";
  confirmOkaytext = "Okay";
  deleteId: any;
  public columns: Array<any> = [
    {
      title: 'Box Barcode',
      name: '_id',
      sort: 'asc',
      className :'table-header-col1'
      // filtering: { filterString: '', placeholder: 'Search Barcode' }
    },
    {
      title: 'Imported Date',
      name: 'importedDate',
      sort: 'asc',
      className :'table-header-col1'
    },
    {
      title: 'Area',
      name: 'area',
      className :'table-header-col2'

    },
    {
      title: 'Supervisor',
      name: 'supervisor',
      className :''

    },
  ];

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    className: ['table-striped', 'table-bordered', "table-sm", "table"]
  };

  public keysOfJson = ["kit", "msisdn", "barcode", "kit sim", "kit box", "area", "supervisor", "importedDate"];

  constructor(private httpService: HttpClient,
    private toastr: ToastrService, public auth: AuthService, private confirmBox: NgxConfirmBoxService, private router: Router) {
      this.onChangeTable(this.config);
      this.user=this.auth.currentUserValue;
      console.log("User",this.user);
      
  }

  ngOnInit(): void {
  }

  pageChanged(event: PageChangedEvent): void {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.returnedArray = this.contentArray.slice(startItem, endItem);
  }


  onFileBrowser(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('stockFile') as HTMLElement;
    element.click();
  }

  convertFile(event) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');

    if (true) {
      let file: File = target.files[0];

      let formData: FormData = new FormData();
      formData.append('uploadFile', file);

      // this.auth.loading = true;
      this.httpService.put(base_url + 'common/bulk/insert/Stock', formData).subscribe((res: any) => {
        console.log(res.message);
        // this.auth.loading = false;
        if (res.code == 200) {
          this.page = 1;
          this.myInputVariable.nativeElement.value = '';
          this.onChangeTable(this.config);
          this.toastr.success(res.message);
        } else {
          this.page = 1;
          this.myInputVariable.nativeElement.value = '';
          this.onChangeTable(this.config);
          this.toastr.error(res.message);
        }

      }, err => {
        this.toastr.error(err.error.message, '');
      });

      /* statuc data*/
    }
  };

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    let sortBy = '';
    let orderBy = {};
    let searchBy = {};
    for (let item of config.sorting.columns) {
      let sort;
      if (item.sort && item.sort.length > 0) {
        sort = item.sort == 'asc' ? 1 : -1;
        sortBy = item.name == '_id' ? 'kit sim' : item.name;
        orderBy = { [sortBy]: sort };
      }

      if (this.searchData && this.searchData.length > 0) {
        searchBy = { 'kit sim': { '$regex': '.*' + this.searchData + '.*', '$options': 'i' } };
      }
    }
    let pageData = page.itemsPerPage * (page.page - 1);


    orderBy = Object.keys(orderBy).length > 0 ? orderBy : {'kit sim' : 1}
    let filterData = { searchBy, orderBy , pageData }
    this.httpService.post(base_url + 'stock/create_list', filterData).subscribe((res: any) => {


      if (res.code == 200) {
        for (let val of res['data']) {
          let date = val.stocks[0].importedDate;
          val.importedDate = moment(date).format('DD-MM-YYYY');
          val.area = val.stocks[0].area;
          val.supervisor = val.stocks[0].supervisor;

        }
        this.returnedArray = res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.returnedArray //this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error("Something went wrong");
      }

    }, err => {
      this.toastr.error("No records found");
    });
  }


  onCellClick(event) {
    console.log("Event on cell", event);
    this.router.navigate(['brick'], { queryParams: { boxId: event.row._id } });

  }

  onBoxSearch(value) {
    console.log(value, "VVVVV");
    this.onChangeTable(this.config);
  }

  yourMethod() {
    this.confirmBox.show();
  }

  OnStockDelete(showConfirm: boolean) {
    if (showConfirm) {
      this.httpService.delete(base_url + 'stock/stock_delete').subscribe((res: any) => {
        console.log(res, "Deleted");
      })
    }
  }
}
