import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StockComponent } from './stock.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StockRoutingModule } from './stock-routing.module';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { AuthGuard, MyInterceptor, DEFAULT_TIMEOUT } from '../../services/guard';
import { Ng2TableModule } from './ng-table-module';
import { NgxConfirmBoxModule, NgxConfirmBoxService } from 'ngx-confirm-box';

@NgModule({
  declarations: [StockComponent],
  imports: [
    StockRoutingModule,
	  NgxConfirmBoxModule,
    FormsModule,
    PaginationModule.forRoot(),
    Ng2TableModule,
    CommonModule
	],
	providers: [
		NgxConfirmBoxService,
    [{ provide: HTTP_INTERCEPTORS, useClass: MyInterceptor, multi: true }],
    [{ provide: DEFAULT_TIMEOUT, useValue: 1800000 }]
	]
})
export class StockModule {}
