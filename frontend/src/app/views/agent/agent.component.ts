import { Component, OnInit, ViewChild, NgZone, ChangeDetectorRef } from "@angular/core";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from "../../services/auth.service";
import { RegexService } from "../../services/regex.service";
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { NgxConfirmBoxService } from 'ngx-confirm-box';

const base_url = environment.apiUrl;

@Component({
  templateUrl: './agent.component.html'
})

export class AgentComponent implements OnInit {

  @ViewChild('form', { static: false }) agentForm: NgForm;
  agentList: any;
  // editData = false;
  emailPattern: RegExp;
  phonePattern: RegExp;

  public rows: Array < any > = [];
  public columns: Array < any > = [{
      title: 'Agent Name',
      name: 'name',
      sort: false,
      //filtering: { filterString: '', placeholder: 'Filter by name' }
    },
    {
      title: 'Address',
      name: 'address',
      sort: false,
      //filtering: { filterString: '', placeholder: 'Filter by address' }
    },
    { title: 'Province', className: ['office-header'], name: 'provinceName', sort: false },
    {
      title: 'Contact person',
      name: 'contactperson',
      sort: false
      //filtering: { filterString: '', placeholder: 'Filter by contact person' }
    },
    { title: 'Contact number', name: 'contactnumber', sort: false },
    { title: 'Contact email', name: 'contactemail', sort: false },
    { title: 'Supervisor', name: 'supervisorName', sort: false }
  ];
  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered', "table-sm", "table", ]
  };


  supervisorData = [];
  provinceData = [];
  deleteId: any;

  public data: Array < any > = [];
  agent = {
    name: '',
    address: '',
    province: '',
    contactperson: '',
    contactnumber: '',
    contactemail: '',
    supervisor: '',
    bankName: '',
    paymentType: '',
    _id: '',
  };
  maxLength: number;
  filterData = { searchBy: { role: 'Supervisor' }, orderBy: { name } }
  searchData: any;
  bankData: any;
  bgColor = 'rgba(0,0,0,0.5)'; // overlay background color
  confirmHeading = '';
  confirmContent = "Are you sure, want to delete this?";
  confirmCanceltext = "Cancel";
  confirmOkaytext = "Okay";
  constructor(
    private ref: ChangeDetectorRef,
    public router: Router,
    private regexService: RegexService,
    private route: ActivatedRoute,
    public auth: AuthService,
    private httpService: HttpClient,
    private toastr: ToastrService,
    private confirmBox: NgxConfirmBoxService,
    private location: Location
  ) {

    this.emailPattern = regexService.pattern.emailPattern;
    this.phonePattern = regexService.pattern.phonePattern;
    this.maxLength = 14;

    //this.GetList();
    // this.httpService.get(base_url + 'common/Agent').subscribe(res => {
    //   this.agentList = res['data'];
    //   this.length = this.agentList.length;
    //   this.data = this.agentList;
    //   this.onChangeTable(this.config);
    // }, err => {
    //   this.toastr.error(err.error.message, '');
    // });
    this.onChangeTable(this.config);
    this.getSupervisorList();
    this.getProvinceData();
    this.getBankData();
  }

  ngOnInit() {

  }

  ///////////////////////////////////////////////////////////////

  keyPress(event: any) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 43) {
      return false;
    }
    return true;
  }
  public changePage(page: any, data: Array < any > = this.data): Array < any > {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  // public changeSort(data: any, config: any): any {
  //   if (!config.sorting) {
  //     return data;
  //   }

  //   let columns = this.config.sorting.columns || [];
  //   let columnName: string = void 0;
  //   let sort: string = void 0;

  //   for (let i = 0; i < columns.length; i++) {
  //     if (columns[i].sort !== '' && columns[i].sort !== false) {
  //       columnName = columns[i].name;
  //       sort = columns[i].sort;
  //     }
  //   }

  //   if (!columnName) {
  //     return data;
  //   }

  //   // simple sorting
  //   return data.sort((previous: any, current: any) => {
  //     if (previous[columnName] > current[columnName]) {
  //       return sort === 'desc' ? -1 : 1;
  //     } else if (previous[columnName] < current[columnName]) {
  //       return sort === 'asc' ? -1 : 1;
  //     }
  //     return 0;
  //   });
  // }

  // public changeFilter(data: any, config: any): any {
  //   let filteredData: Array < any > = data;
  //   this.columns.forEach((column: any) => {
  //     if (column.filtering) {
  //       filteredData = filteredData.filter((item: any) => {
  //         return (item[column.name].toLowerCase()).match(column.filtering.filterString.toLowerCase());
  //       });
  //     }
  //   });

  //   if (!config.filtering) {
  //     return filteredData;
  //   }

  //   if (config.filtering.columnName) {
  //     return filteredData.filter((item: any) =>
  //       (item[config.filtering.columnName].toLowerCase()).match(this.config.filtering.filterString.toLowerCase()));
  //   }

  //   let tempArray: Array < any > = [];
  //   filteredData.forEach((item: any) => {
  //     let flag = false;
  //     this.columns.forEach((column: any) => {
  //       if ((item[column.name].toString().toLowerCase()).match(this.config.filtering.filterString.toLowerCase())) {
  //         flag = true;
  //       }
  //     });
  //     if (flag) {
  //       tempArray.push(item);
  //     }
  //   });
  //   filteredData = tempArray;

  //   return filteredData;
  // }

  public onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
    var sortBy = '';
    var orderBy = {};
    var searchBy = {};
    for (let item of config.sorting.columns) {

      // if (item.sort && item.sort.length > 0) {
      //   sortBy = item.name
      //   orderBy = { [sortBy]: item.sort };
      // }

      if (item.filtering && item.filtering.filterString.length > 0) {
        if (item.filtering.placeholder == "Filter by name") {
          searchBy = { name: { '$regex': '.*' + item.filtering.filterString + '.*' } };
        } else if (item.filtering.placeholder == "Filter by address") {
          searchBy = { address: { '$regex': '.*' + item.filtering.filterString + '.*' } };
        } else if (item.filtering.placeholder == "Filter by contact person") {
          searchBy = { contactperson: { '$regex': '.*' + item.filtering.filterString + '.*' } };
        }
      }
      if (this.searchData && this.searchData.length > 0) {
        searchBy = { name: { '$regex': '.*' + this.searchData + '.*', '$options': 'i' } };
      }
    }
    let pageData = page.itemsPerPage * (page.page - 1);
    let filterData = { searchBy, orderBy, pageData }
    this.httpService.post(base_url + 'common/Agent', filterData).subscribe((res: any) => {
      if (res.code == 200) {
        // console.log(res.data);

        this.agentList = res['data'];
        if (config.filtering) {
          Object.assign(this.config.filtering, config.filtering);
        }

        if (config.sorting) {
          Object.assign(this.config.sorting, config.sorting);
        }

        let filteredData = this.agentList //this.changeFilter(this.returnedArray, this.config);

        this.rows = filteredData; //page && config.paging ? this.changePage(page, filteredData) : filteredData;

        this.length = res.totalCount;

      } else {
        this.toastr.error("Something went wrong");
      }

    }, err => {
      this.toastr.error(err.error.message, '');
    });
    // if (config.filtering) {
    //   Object.assign(this.config.filtering, config.filtering);
    // }

    // if (config.sorting) {
    //   Object.assign(this.config.sorting, config.sorting);
    // }
    // // let filteredData = this.changeFilter(this.data, this.config);

    // // let sortedData = this.changeSort(filteredData, this.config);
    // this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    // this.length = sortedData.length;
  }

  onAgentSearch(value) {
    this.onChangeTable(this.config);
  }

  public onCellClick(data: any): any {}

  public onEditAgentData(event: any) {
    this.auth.editData = true;
    this.httpService.get(base_url + 'common/Agent/' + event["event"]).subscribe((res: any) => {
      this.agent = res.data;
    })
  }
  public onEditAgentBoxData(event: any) {
    this.router.navigate(['add-box'], { queryParams: { agentId: event["event"]._id, supervisorId: event["event"].supervisor } });
  }
  public onViewBoxData(event: any) {
    this.router.navigate(['view-boxes'], { queryParams: { agentId: event["event"]._id } });
  }

  getSupervisorList() {
    this.httpService.post(base_url + 'common/User', this.filterData).subscribe((res: any) => {
      this.supervisorData = res.data;
    })
  }

  getProvinceData() {
    this.httpService.get(base_url + 'common/Province').subscribe((res: any) => {
      this.provinceData = res.data;
    })
  }

  getBankData() {
    this.httpService.get(base_url + 'common/Bank').subscribe((res: any) => {
      this.bankData = res.data;
    })
  }

  editDatashow(data) {
    this.auth.editData = true;
    this.agent = data;
  }

  GetList() {

    this.httpService.get(base_url + 'common/Agent').subscribe(res => {
      this.agentList = res['data'];
      this.length = this.agentList.length;
      this.data = this.agentList;
    }, err => {
      this.toastr.error(err.error.message, '');
    });
  }

  addData() {
    this.auth.editData = true;
    this.location.replaceState('/agent/add');

  }

  resetData(agentForm) {
    this.auth.editData = false;
    agentForm.reset();
    this.GetList();
    this.location.replaceState('/agent');

  }


  yourMethod(id) {
    this.deleteId = id.event;
    this.confirmBox.show();
  }

  deleteData(showConfirm: boolean) {

    if (showConfirm) {
      console.log(this.deleteId, "this.deleteId")
      this.httpService.delete(base_url + 'common/Agent/' + this.deleteId).subscribe((res1: any) => {
        this.GetList();
        this.toastr.success('Agent Deleted', '');
        console.log(res1)
        if (res1.code == 200) {
          window.location.reload();
        }
        this.ref.detectChanges();
      }, err => {
        this.GetList();
        this.toastr.error(err.error.message, '');
        this.ref.detectChanges();
        window.location.reload();
      });
    }
  }

  submit(agentForm) {
    if (agentForm.form.status == "INVALID") {
      Object.keys(agentForm.controls).forEach(key => {
        agentForm.controls[key].markAsDirty();
      });
      return false;
    }

    if (this.agent._id) {
      this.httpService.post(base_url + 'common/Agent/' + this.agent._id, this.agent).subscribe((res1: any) => {
        agentForm.reset();
        // this.GetList();
        this.onChangeTable(this.config);
        this.toastr.success('Agent Updated');
        this.auth.editData = false;
      }, err => {
        this.auth.editData = true;
        this.toastr.error(err.error.message, '');
      });

    } else {

      delete this.agent._id;

      if (this.agent.bankName == "")
        delete this.agent.bankName

      if (this.agent.contactemail == "")
        delete this.agent.contactemail

      console.log("this.agent-=-=-=-=")
      console.log(this.agent)
      this.httpService.put(base_url + 'common/Agent', this.agent).subscribe((res1: any) => {
        agentForm.reset();
        // this.GetList();
        this.onChangeTable(this.config);

        this.toastr.success('Agent Added');
        this.auth.editData = false;
        this.location.replaceState('/agent');
      }, err => {
        this.auth.editData = true;
        this.toastr.error(err.error.message, '');
      });

    }

  }

}
