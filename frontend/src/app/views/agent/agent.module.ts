import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgentRoutingModule } from './agent.routing.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { Ng2TableModule } from './ng-table-module';
import { AgentBoxComponent } from '../agent-box/agent-box.component';
import { AgentComponent } from './agent.component';
import { NgxConfirmBoxModule, NgxConfirmBoxService } from 'ngx-confirm-box';

@NgModule({
  declarations: [AgentComponent],
  imports: [
    AgentRoutingModule,
    NgxConfirmBoxModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    FormsModule,
    Ng2TableModule,
    CommonModule
  ], 
  providers: [
    NgxConfirmBoxService
  ]
})
export class AgentModule {}
