import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import * as crypto from 'crypto-js';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { constant } from '../constant';
// import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../../../src/app/models/user';
const base_url = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userData: any;
  editData = false;
  currentUser: any;
  loading = false;
  isProfileComplete: boolean = false;
  private currentUserSubject: BehaviorSubject<User>;
  public loginUser: Observable<User>;
  constructor(private httpService: HttpClient, public router: Router) {

    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  checkUserRole() {    
    if (this.currentUserValue.role == "Super-Admin") {
      return true;
    } else {
      return false;
    }
  }
  logOut() {
    localStorage.removeItem('authToken');
    localStorage.removeItem('currentUser');
    this.currentUser = {};
    this.router.navigate(['/login']);
  }

  isLoggedIn() {
    const localData = localStorage.getItem('authToken');
    if (localData && localData.length > 0) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      return true;
    }
    return false;
  }

  Tokenverify() {
    const localData = localStorage.getItem('authToken');
    return this.httpService.post(base_url + constant.VERIFY_TOKEN, localData)
  }

  checkLogin(obj) {
    obj.password = crypto.AES.encrypt(obj.password, environment.encryptionKey).toString();
    return this.httpService.post(base_url + constant.LOGIN, obj)
      .pipe(map(user => {
        if (user) {
          this.userData = user;
          // store user details local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(this.userData.data.user));
          this.currentUserSubject.next(this.userData.data.user);
          this.currentUser = this.userData.data;
        }
        return user;
      }));
  }

  userSignup(obj) {
    obj.password = crypto.AES.encrypt(obj.password, environment.encryptionKey).toString();
    return this.httpService.post(base_url + constant.SIGNUP, obj)
  }

  updateUser(obj) {
    return this.httpService.post(base_url + constant.UPDATE_USER, obj)
  }

  updateAdmin(obj) {
    return this.httpService.post(base_url + constant.UPDATE_ADMIN, obj)
  }

  getUserSingle(obj) {
    return this.httpService.post(base_url + constant.GET_USER_DETAIL, obj)
  }

  checkCurrentPass(pass) {
    var objdata = {
      userid: pass.userId,
      password: crypto.AES.encrypt(pass.password, environment.encryptionKey).toString(),
    }
    return this.httpService.post(base_url + constant.CHECK_CURRENT_PASS, objdata)
  }

  updatePassUser(pass) {
    var chanPass = {
      userid: pass.userId,
      password: crypto.AES.encrypt(pass.password, environment.encryptionKey).toString(),
    }
    return this.httpService.post(base_url + constant.UPDATE_PASS, chanPass)
  }

  recoverPass(obj) {
    return this.httpService.post(base_url + constant.FORGOTPASSWORD, obj)
  }

  changePass(obj) {
    return this.httpService.post(base_url + constant.RESETPASSWORD, obj)
  }

}
