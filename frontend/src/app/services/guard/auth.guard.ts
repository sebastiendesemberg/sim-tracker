import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable < boolean > | Promise < boolean > | boolean {
    if (!window['maintenance']) {

      if (next.routeConfig.path === 'login' || next.routeConfig.path === 'register') {
        if (this.auth.isLoggedIn()) {
          this.router.navigate(['users']);
          return false;
        }
        return true;
      } else {
        if (this.auth.isLoggedIn() && next.routeConfig.path === 'maintenance') {
          this.router.navigate(['users']);
          return true;
        }
        if (this.auth.isLoggedIn()) {
          return true;
        }
        // this.router.navigate(['users']);
        return this.auth.isLoggedIn();
      }
    } else {
      if (next.routeConfig.path !== 'maintenance') {
        this.router.navigate(['maintenance']);
      }
      return true;
    }


  }
}
