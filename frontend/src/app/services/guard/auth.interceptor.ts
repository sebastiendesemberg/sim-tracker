// The HttpInterceptor is an interface and used to implement the intercept method.

import { Inject, Injectable, InjectionToken } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { timeout } from 'rxjs/operators';

export const DEFAULT_TIMEOUT = new InjectionToken<number>('defaultTimeout');

@Injectable()
export class MyInterceptor implements HttpInterceptor {

  constructor(@Inject(DEFAULT_TIMEOUT) protected defaultTimeout: number,private auth: AuthService, public router: Router, ) {}
  intercept(req: HttpRequest < any > , next: HttpHandler): Observable < HttpEvent < any >> {
    const timeoutValue = req.headers.get('timeout') || this.defaultTimeout;
    const timeoutValueNumeric = Number(timeoutValue);
    this.auth.loading = true;
    if (req.url.includes('/verify/email')) {
      this.auth.loading = false;
    } else {
      this.auth.loading = true;
    }

    if (localStorage.getItem('authToken')) {
      const reqHeader = req.clone({ headers: req.headers.set('Authorization', 'JWT ' + localStorage.getItem('authToken')) });
    return next.handle(reqHeader).pipe(timeout(timeoutValueNumeric),map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // console.log('27')
           this.auth.loading = false;
        }
       //  this.auth.loading = false;
        return event;
      }), catchError((err: any, caught) => {

        if (err instanceof HttpErrorResponse) {
          if (err.status === 0) {
            this.router.navigate(['maintenance']);
          }
          // console.log('39')
          this.auth.loading = false;
          return throwError(err);
        }
      }))
    } else {
    return next.handle(req).pipe(timeout(timeoutValueNumeric),map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('47')
          this.auth.loading = false;
        }
        return event;
      }), catchError((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 0) {
            this.router.navigate(['maintenance']);
          }
          console.log('56')
          this.auth.loading = false;
          return throwError(err);
        }
      }));
    }
  }
}
