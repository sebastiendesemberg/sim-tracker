/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */
// Insert seed models below
const bcrypt = require("bcrypt");
var user = require("../models/user");
var stocks = require("../models/stock");
var Province = require("../models/province");
var Banks = require("../models/bank");

bcrypt.hash("S!mtracker1", 10, (err, hash) => {
    var admin = new user({
        name: "Classic",
        surName: "Tracker",
        cell: "1234567890",
        email: "simtracker@ledimo.co.za",
        password: hash,
        active: true,
        role: "Super-Admin"
    });
    user.findOne({
            email: "simtracker@ledimo.co.za"
        },
        function(error, docs) {
            if (!docs) {
                user.create(admin, function(err, user) {
                    if (err) {
                        console.log("Error creating default admin : " + err);
                    }
                });
            }
        }
    );
});

var provinceCreate = [{
        name: "Eastern Cape",
        value: "easternCape"
    },
    {
        name: "Free State",
        value: "freeState"
    },
    {
        name: "Gauteng",
        value: "gauteng"
    },
    {
        name: "KwaZulu-Natal",
        value: "kwaZuluNatal"
    },
    {
        name: "Limpopo",
        value: "limpopo"
    },
    {
        name: "Mpumalanga",
        value: "mpumalanga"
    },
    {
        name: "Northern Cape",
        value: "northernCape"
    },
    {
        name: "North-West",
        value: "northWest"
    },
    {
        name: "Western Cape",
        value: "westernCape"
    }
];

Province.find({}, function(err, data) {
    if (data.length == 0) {
        Province.insertMany(provinceCreate, function(err, docs) {
            if (err) {
                console.log("Error creating roles : " + err);
            }
        });
    }
});

var banks = [{
        name: "Absa Group Limited",
        value: "Absa Group Limited"
    },
    {
        name: "African Bank Limited",
        value: "African Bank Limited"
    },
    {
        name: "Bidvest Bank Limited",
        value: "Bidvest Bank Limited"
    },
    {
        name: "Capitec Bank Limited",
        value: "Capitec Bank Limited"
    },
    {
        name: "Discovery Limited",
        value: "Discovery Limited"
    },
    {
        name: "First National Bank",
        value: "First National Bank"
    },
    {
        name: "FirstRand Bank - A subsidiary of First Rand Limited",
        value: "FirstRand Bank - A subsidiary of First Rand Limited"
    },
    {
        name: "Grindrod Bank Limited",
        value: "Grindrod Bank Limited"
    },
    {
        name: "Imperial Bank South Africa",
        value: "Imperial Bank South Africa"
    },
    {
        name: "Investec Bank Limited",
        value: "Investec Bank Limited"
    },
    {
        name: "Nedbank Limited",
        value: "Nedbank Limited"
    },
    {
        name: "Sasfin Bank Limited",
        value: "Sasfin Bank Limited"
    },
    {
        name: "Standard Bank of South Africa",
        value: "Standard Bank of South Africa"
    },
    {
        name: "Ubank Limited",
        value: "Ubank Limited"
    },
    {
        name: "TymeBank",
        value: "TymeBank"
    }
];

Banks.find({}, function(err, data) {
    if (data.length == 0) {
        Banks.insertMany(banks, function(err, docs) {
            if (err) {
                console.log("Error creating banks : " + err);
            }
        });
    }
});