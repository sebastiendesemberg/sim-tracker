const mongoose = require('mongoose');
const provinceSchema = mongoose.Schema({

    name: {
        type: String
    },
    value: { type: String },

    createdDate: {
        type: Date,
        default: Date.now()
    },
    lastUpdatedDate: {
        type: Date,
        default: Date.now()
    },

});



provinceSchema.index({
    name: 1
}, {
    unique: true,
});

module.exports = mongoose.model('Province', provinceSchema);