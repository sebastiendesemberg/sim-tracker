const mongoose = require('mongoose');

const userSchema = mongoose.Schema({

    name: {
        type: String
    },
    surName: {
        type: String
    },
    cell: {
        type: String
    },
    area: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: {
        type: String
    },
    active: {
        type: Boolean,
        default: false
    },
    role: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now()
    },
    lastUpdatedDate: {
        type: Date,
        default: Date.now()
    },
});


userSchema.index({
    cell: 1,
    email: 1
}, {
    unique: true,
});



module.exports = mongoose.model('User', userSchema);