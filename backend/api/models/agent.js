const mongoose = require('mongoose');

const agentSchema = mongoose.Schema({

    name: String,
    address: String,
    province: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Province'
    },
    provinceName: String,
    supervisorName: String,
    bankName: {
        type: String
    },
    paymentType: {
        type: String
    },
    contactperson: {
        type: String
    },
    contactnumber: {
        type: String
    },
    contactemail: String,
    supervisor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    assignedBoxes: {
        type: Array
    },
    assignedBrickes: {
        type: Array
    },
    sumCom: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now()
    },
    lastUpdatedDate: {
        type: Date,
        default: Date.now()
    },
});


module.exports = mongoose.model('agents', agentSchema);