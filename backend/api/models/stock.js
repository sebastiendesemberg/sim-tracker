const mongoose = require('mongoose');

const stockSchema = mongoose.Schema({

    kit: {
        type: String
    },
    msisdn: {
        type: String
    },
    barcode: {
        type: String
    },
    'kit sim': {
        type: String
    },
    'kit box': {
        type: String
    },
    active: {
        type: Boolean,
        default: false
    },
    area: {
        type: String,
        default: ""
    },
    supervisor: {
        type: String,
        default: ""
    },
    supervisorId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    bankName:{
        type:String,
        default: ""
    },
    areaId:{
        type: String,
        default: ""
    },
    kit_brick: {
        type: String
    },
    importedDate: {
        type: Date,
        default: () => { return new Date() }
    },
    lastUpdatedDate: {
        type: Date,
        default: () => { return new Date() }
    },
});


stockSchema.index({
    kit: 1
}, {
    unique: true,
});


module.exports = mongoose.model('Stock', stockSchema);