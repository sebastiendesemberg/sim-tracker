const mongoose = require('mongoose');
const areaSchema = mongoose.Schema({

    name: {
        type: String
    },
    //value: { type: String },
    userCounts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    createdDate: {
        type: Date,
        default: Date.now()
    },
    lastUpdatedDate: {
        type: Date,
        default: Date.now()
    },

});



areaSchema.index({
    name: 1
}, {
    unique: true,
});

module.exports = mongoose.model('Area', areaSchema);