const mongoose = require('mongoose');

const reportSchema = mongoose.Schema({
    PRIMARY_GRP: {
        type: String
    },
    PROV_FIRST_CONN: {
        type: String
    },
    City_First_Conn: {
        type: String
    },
    CUSTOMER_NAME: {
        type: String
    },
    ADDRESS1: {
        type: String
    },
    PROVINCE: {
        type: String
    },
    REGION_NAME: {
        type: String
    },
    CATEGORY_SET_NAME: {
        type: String
    },
    CAT_SEGMENT2: {
        type: String
    },
    CAT_SEGMENT3: {
        type: String
    },
    DISTRIBUTION_DATE: {
        type: String
    },
    MSISDN: {
        type: String
    },
    SIM: {
        type: String
    },
    SERIAL_NUMBER: {
        type: String
    },
    TRX_NUMBER: {
        type: String
    },
    BOX_SERIAL_NUMBER: {
        type: String
    },
    BRICK_SERIAL_NUMBER: {
        type: String
    },
    ACTIVATION_DATE: {
        type: String
    },
    ACTIVATION_MYEAR: {
        type: String
    },
    COMM_MTH: {
        type: String
    },
    usage_spent_4mths: {
        type: String
    },
    usage_spent_7mths: {
        type: String
    },
    voice_amt_4mths: {
        type: String
    },
    gprsamt_4mths: {
        type: String
    },
    total_bundle_purchase_amt_4mths: {
        type: String
    },
    COMM_EXCL: {
        type: String
    },
    VAT: {
        type: String
    },
    COMM_INCL: {
        type: String
    },

    importedDate: {
        type: Date,
        // default: () => {
        //     return new Date()
        // }
    },
    lastUpdatedDate: {
        type: Date,
        default: () => {
            return new Date()
        }
    },
});


// reportSchema.index({
//     // SERIAL_NUMBER: 1,
//     importedDate:1
// }, {
//     unique: true,
// });


module.exports = mongoose.model('Report', reportSchema);