const mongoose = require('mongoose');
const bankSchema = mongoose.Schema({

    name: {
        type: String
    },
    value: { type: String },
    createdDate: {
        type: Date,
        default: Date.now()
    },
    lastUpdatedDate: {
        type: Date,
        default: Date.now()
    },

});



bankSchema.index({
    name: 1
}, {
    unique: true,
});

module.exports = mongoose.model('Bank', bankSchema);