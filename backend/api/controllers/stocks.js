const Stock = require('../models/stock');
const User = require('../models/user');
const Area = require('../models/area');

exports.stock_post = (req, res, next) => {
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;

    console.log(req.body)
    sortBy = req.body.orderBy ? req.body.orderBy : { '_id': 1 };
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;
    // Stock.find(searchBy).skip(pageData).limit(limit).sort(sortBy).exec(function(err, annos) {
    Stock.aggregate([{
            "$match": searchBy
        },
        {
            $group: {
                _id: "$kit sim",
                stocks: {
                    $push: "$$ROOT"
                }
            }
        },
        { "$sort": sortBy },
        {
            "$skip": pageData
        },
        {
            "$limit": limit
        },
    ]).allowDiskUse(true).exec(function(err, annos) {
        console.log(err);

        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {

            // Stock.find(searchBy).count().exec(function(err, countData) {
            Stock.aggregate([{
                    "$match": searchBy
                },
                {
                    $group: {
                        _id: "$kit sim",
                        stocks: {
                            $push: "$$ROOT"
                        }
                    }
                },
                //  { "$sort": { "score": { "$meta": "textScore" } } },
                // { "$skip": pageData },
                // { "$limit": limit },
                {
                    $count: "totalCount"
                }

            ]).allowDiskUse(true).exec(function(err, countData) {
                // console.log(countData[0].totalCount);
                if (countData.length == 0) {
                    countData = 0
                } else {
                    countData = countData[0].totalCount
                }
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "Data Not Found",
                        data: [],
                        error: err
                    });
                } else {
                    // console.log(annos)
                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: annos,
                        totalCount: countData,
                        error: []
                    });
                }
            });



            // Area.find({}).exec(function(err, areaData) {
            //     console.log(areaData, "areaData")

            //     annos.forEach(function(data) {
            //         data.stocks.forEach(function(stocksData) {
            //             areaData.forEach(function(areaDt) {
            //                 if (areaDt._id == stocksData.area) {
            //                     stocksData.area = areaDt.name
            //                 }
            //             });
            //         });
            //     });

            // })
        }





        // Area.populate(annos, { path: '_id' }, function(err, populatedData) {
        //     console.log("Poppulated data", populatedData);

        // });

    })
}

exports.brick_post = (req, res, next) => {
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;

    sortBy = req.body.orderBy ? req.body.orderBy : {};
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;
    console.log("Search data", searchBy, sortBy);

    // Stock.find(searchBy).skip(pageData).limit(limit).sort(sortBy).exec(function(err, annos) {
    Stock.aggregate([{
            "$match": searchBy
        },
        {
            $group: {
                _id: "$kit box",
                stocks: {
                    $push: "$$ROOT"
                }
            }
        },
        { "$sort": sortBy },
        {
            "$skip": pageData
        },
        {
            "$limit": limit
        },
    ]).exec(function(err, annos) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {


            // Stock.find(searchBy).count().exec(function(err, countData) {
            Stock.aggregate([{
                    "$match": searchBy
                },
                {
                    $group: {
                        _id: "$kit box",
                        stocks: {
                            $push: "$$ROOT"
                        }
                    }
                },
                //  { "$sort": { "score": { "$meta": "textScore" } } },
                // { "$skip": pageData },
                // { "$limit": limit },
                {
                    $count: "totalCount"
                }

            ]).exec(function(err, countData) {
                // console.log(countData[0].totalCount);
                if (countData.length == 0) {
                    countData = 0
                } else {
                    countData = countData[0].totalCount
                }
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "Data Not Found",
                        data: [],
                        error: err
                    });
                } else {
                    // console.log(annos)
                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: annos,
                        totalCount: countData,
                        error: []
                    });
                }
            });

        }
    })
}

exports.pack_post = (req, res, next) => {
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;

    sortBy = req.body.orderBy ? req.body.orderBy : {};
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;
    console.log("Search data", searchBy, sortBy);

    // Stock.find(searchBy).skip(pageData).limit(limit).sort(sortBy).exec(function(err, annos) {
    Stock.aggregate([{
            "$match": searchBy
        },
        {
            $group: {
                _id: "$kit",
                stocks: {
                    $push: "$$ROOT"
                }
            }
        },
        { "$sort": sortBy },
        //  { "$sort": { "score": { "$meta": "textScore" } } },
        {
            "$skip": pageData
        },
        {
            "$limit": limit
        },
    ]).exec(function(err, annos) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {


            // Stock.find(searchBy).count().exec(function(err, countData) {
            Stock.aggregate([{
                    "$match": searchBy
                },
                {
                    $group: {
                        _id: "$kit",
                        stocks: {
                            $push: "$$ROOT"
                        }
                    }
                },
                //  { "$sort": { "score": { "$meta": "textScore" } } },
                // { "$skip": pageData },
                // { "$limit": limit },
                {
                    $count: "totalCount"
                }

            ]).exec(function(err, countData) {
                console.log(countData, "Count");
                if (countData.length == 0) {
                    countData = 0
                } else {
                    countData = countData[0].totalCount
                }
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "Data Not Found",
                        data: [],
                        error: err
                    });
                } else {
                    // console.log(annos)
                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: annos,
                        totalCount: countData,
                        error: []
                    });
                }
            });

        }
    })
}


exports.stock_delete = (req, res, next) => {
    Stock.deleteMany({}, function(err, area) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: err.errmsg,
                data: [],
                error: err.errmsg
            });
        } else {
            return res.status(200).json({
                status: "OK",
                code: 200,
                message: `Stock Deleted`,
                data: [],
                error: []
            });
        }
    });

};


// for updating dispatch based on supervisor
exports.update_dispatch = async (req, res, next) => {

    try {
        const {
            area,
            boxId,
            supervisor,
            supervisorId
        } = req.body;
        console.log(req.body, "===body===")
        // let boxId = ["CJA0812501", "CJA0812343", "CJA0812234"];
        // let boxId=boxId;
        let areaName;
        Area.find({ _id: area }).exec(async function(err, areaData) {
            if (err) {
                console.log(err);

            } else {
                areaName = areaData[0].name;
                const stock = await Stock.update({
                    "kit sim": {
                        $in: boxId
                    }
                }, {
                    $set: {
                        "supervisor": supervisor,
                        "supervisorId": supervisorId,
                        "area": areaName,
                        "areaId": area
                    }
                }, {
                    multi: true
                });
                // await stock.save();
                if (!stock) {
                    return res.status(401).json({
                        status: "ERROR",
                        code: 401,
                        message: "User not found",
                        data: [],
                        error: []
                    });
                }
                return res.status(200).json({
                    status: "OK",
                    code: 200,
                    message: "Dispatch Updated",
                    data: [],
                    error: []
                });
            }
        });

    } catch (err) {
        console.log("eerrrr", err);

        return res.status(500).json({
            status: "ERROR",
            code: 500,
            message: "Error in update",
            data: [],
            error: err
        });
    }


    // Stock.findById(barcode, function(err, thing) {
    //     if (err) {
    //         return res.status(401).json({
    //             status: "ERROR",
    //             code: 401,
    //             message: "User not found",
    //             data: [],
    //             error: err
    //         });
    //     }
    //     if (!thing) {
    //         return res.status(401).json({
    //             status: "ERROR",
    //             code: 401,
    //             message: "User not found",
    //             data: [],
    //             error: []
    //         });
    //     }
    //     var updated = _.assign(thing, req.body);
    //     if (!updated) {
    //         return res.status(401).json({
    //             status: "ERROR",
    //             code: 401,
    //             message: "User not found",
    //             data: [],
    //             error: []
    //         });
    //     }
    //     updated.save(function(err) {
    //         if (err) {
    //             if (err.name === 'MongoError' && err.code === 11000) {
    //                 return res.status(401).json({
    //                     status: "ERROR",
    //                     code: 401,
    //                     message: "Sequence type should be unique",
    //                     data: [],
    //                     error: []
    //                 });
    //             } else {
    //                 return res.status(401).json({
    //                     status: "ERROR",
    //                     code: 401,
    //                     message: "Error Occure",
    //                     data: [],
    //                     error: err
    //                 });
    //             }
    //         } else {
    //             return res.status(200).json({
    //                 status: "OK",
    //                 code: 200,
    //                 message: "Dispatch Updated",
    //                 data: [],
    //                 error: []
    //             });
    //         }
    //     });
    // });
}