    const mongoose = require("mongoose");
    const bcrypt = require("bcrypt");
    const jwt = require("jsonwebtoken");
    const CryptoJS = require("crypto-js");
    const async = require("async");
    const fs = require('fs');
    const _ = require('lodash');
    const Agent = require("../models/agent");
    const Area = require("../models/area");
    const Stock = require("../models/stock");
    const Bank = require("../models/bank");
    const User = require("../models/user");
    const Report = require("../models/report");
    const XLSX = require("xlsx");
    var ObjectId = require('mongodb').ObjectId;
    var ISODate = require('mongodb').ISODate;

    const pdf = require('express-pdf');

    exports.generate_report_get_stock_count = (req, res, next) => {
        let reportDetail = {
            filterType: req.body.filterType,
            startDate: new Date(req.body.startDate),
            endDate: new Date(req.body.endDate)
        }

        reportDetail.startDate.setDate(reportDetail.startDate.getDate() + 1)

        var stDate = new Date(req.body.startDate);
        var stMonth = reportDetail.startDate.getMonth() + 1;
        var stYear = reportDetail.startDate.getFullYear();
        var enDate = reportDetail.endDate;
        var edMonth = reportDetail.endDate.getMonth() + 1;
        var edYear = reportDetail.endDate.getFullYear();


        stMonth = stMonth <= 9 ? '0' + stMonth : stMonth;
        stDate = `${stYear}-${stMonth}-01`;
        edMonth = edMonth <= 9 ? '0' + edMonth : edMonth;
        enDate = `${edYear}-${edMonth}-31`;

        let uasadate = reportDetail.startDate;
        let uaeadate = reportDetail.endDate;
        let uastartDate = (uasadate.getUTCFullYear()) + "-" + (uasadate.getUTCMonth() + 1) + "-" + "01 00:00:00";

        uastartDate = new Date(uastartDate).getTime();

        var uaeayear = (uaeadate.getUTCFullYear());
        var uaeamonth = (uaeadate.getUTCMonth() + 1);
        var ualast_date = new Date(uaeayear, uaeamonth, 0).getDate();
        let uaendDate = uaeayear + "-" + uaeamonth + "-" + ualast_date + " 23:59:59";
        uaendDate = new Date(uaendDate).getTime();

        Report.find({
            importedDate: {
                $gte: stDate,
                $lte: enDate
            }
        }).exec(function(err, reports) {

            if (err) {
                return res.status(400).json({
                    status: "ERROR",
                    code: 400,
                    message: 'Bad Request',
                    data: [],
                    error: 'Bad Request'
                });
            } else {
                console.log("1")
                let unboxArray = [],
                    uboxIdList = [];
                reports.forEach(function(unboxData) {
                    let existData = unboxArray.filter(x => x == unboxData.BOX_SERIAL_NUMBER);
                    if (existData && existData.length == 0) {
                        unboxArray.push(unboxData.BOX_SERIAL_NUMBER);
                    }
                })

                Stock.countDocuments({
                    bankName: {
                        $eq: ""
                    }
                }).exec(function(errs, ureport) {
                    console.log("ureport=--=-=-=-=")
                    console.log(ureport)

                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: ureport,
                        error: []
                    });

                })
            }
        });
    }

    exports.generate_report = (req, res, next) => {

        let reportDetail = {
            filterType: req.body.filterType,
            startDate: new Date(req.body.startDate),
            endDate: new Date(req.body.endDate)
        }

        reportDetail.startDate.setDate(reportDetail.startDate.getDate() + 1)

        var stDate = new Date(req.body.startDate);
        var stMonth = reportDetail.startDate.getMonth() + 1;
        var stYear = reportDetail.startDate.getFullYear();
        var enDate = reportDetail.endDate;
        var edMonth = reportDetail.endDate.getMonth() + 1;
        var edYear = reportDetail.endDate.getFullYear();


        stMonth = stMonth <= 9 ? '0' + stMonth : stMonth;
        stDate = `${stYear}-${stMonth}-01`;
        edMonth = edMonth <= 9 ? '0' + edMonth : edMonth;
        enDate = `${edYear}-${edMonth}-31`;

        let uasadate = reportDetail.startDate;
        let uaeadate = reportDetail.endDate;
        let uastartDate = (uasadate.getUTCFullYear()) + "-" + (uasadate.getUTCMonth() + 1) + "-" + "01 00:00:00";

        uastartDate = new Date(uastartDate).getTime();

        var uaeayear = (uaeadate.getUTCFullYear());
        var uaeamonth = (uaeadate.getUTCMonth() + 1);
        var ualast_date = new Date(uaeayear, uaeamonth, 0).getDate();
        let uaendDate = uaeayear + "-" + uaeamonth + "-" + ualast_date + " 23:59:59";
        uaendDate = new Date(uaendDate).getTime();

        Report.find({
            importedDate: {
                $gte: stDate,
                $lte: enDate
            }
        }).exec(function(err, reports) {

            if (err) {
                return res.status(400).json({
                    status: "ERROR",
                    code: 400,
                    message: 'Bad Request',
                    data: [],
                    error: 'Bad Request'
                });
            } else {
                console.log("1")
                let unboxArray = [],
                    uboxIdList = [];
                reports.forEach(function(unboxData) {
                    let existData = unboxArray.filter(x => x == unboxData.BOX_SERIAL_NUMBER);
                    if (existData && existData.length == 0) {
                        unboxArray.push(unboxData.BOX_SERIAL_NUMBER);
                    }
                })
                console.log("2")

                Stock.find({
                    "kit sim": {
                        $in: unboxArray
                    },
                    bankName: {
                        $eq: ""
                    }
                }).skip(req.body.skip).limit(req.body.limit).exec(function(errs, ureport) {
                    console.log("3")
                    unboxArray.forEach(function(undistict) {
                        let existUBData = ureport.filter(x => x["kit sim"] == undistict);
                        if (existUBData && existUBData.length > 0) {
                            uboxIdList.push(undistict);
                        }
                    })
                    console.log("4")
                    if (ureport.length == 0) {
                        unboxArray.forEach(function(undistict) {
                            uboxIdList.push(undistict);
                        })
                    }
                    console.log("5")
                    var totalsim = [];

                    if (req.body.filterType == 'area') {
                        console.log("6")
                        User.find({
                            "role": "Supervisor"
                        }).exec(function(err, supervisorData) {
                            Agent.find({}).exec(function(err, superAgentData) {

                                let superArray = [];
                                let areaArray = [];
                                let areaNameArray = [];
                                console.log("7")
                                if (supervisorData.length > 0) {

                                    supervisorData.forEach(function(svData) {
                                        superArray.push(svData._id);
                                        let existCheck = areaArray.filter(x => x == svData.area);
                                        if (existCheck && existCheck.length == 0) {
                                            areaArray.push(svData.area);
                                        }
                                    })
                                    console.log("8")
                                    if (areaArray.length > 0) {
                                        Area.find({
                                            _id: {
                                                $in: areaArray
                                            }
                                        }).exec(function(err, areaData) {
                                            areaData.forEach(function(areaObj) {
                                                areaNameArray.push({
                                                    _id: areaObj._id,
                                                    name: areaObj.name
                                                });
                                            });
                                            console.log("9")
                                            var template = '';
                                            let areaWiseSupervisor = [];
                                            if (req.body.skip == 0) {
                                                template = `<table class="table dataTable table-striped table-bordered table-sm commission-table" id="commission-table"><thead><tr><td colspan="7" class="bg-primary font-weight-bold">Date Range: ${req.body.startDate} -  ${req.body.endDate}</td></tr></thead><tbody>`;
                                            }

                                            areaNameArray.forEach(function(areaData) {

                                                var areaname = areaData.name;
                                                areaData = areaData._id;

                                                template = template + "<tr><td colspan='7'></td></tr><tr><td colspan='7' class='font-weight-bold'>AREA:" + areaname.toString().toUpperCase() + "</td></tr>";
                                                areaWiseSupervisor = supervisorData.filter(x => x.area == areaData)

                                                let areaTotal = 0;

                                                areaWiseSupervisor.forEach(function(superV) {

                                                    template = template + "<tr><td colspan='7'>" + superV.name + "</td></tr>";

                                                    template = template + "<tr><th></th><th>Agent Name</th> <th>Phone No</th> <th>Bank Name</th> <th>Payment Type</th><th>Total Sims</th><th>Commission</th></tr>";

                                                    let superVtotal = 0;
                                                    superAgentData.forEach(function(agentD) {

                                                        agentD['totalSim'] = 0;
                                                        agentD['sumCom'] = 0;
                                                        agentD.assignedBoxes.forEach(function(boxDt, index) {

                                                            let sadate = reportDetail.startDate;
                                                            let eadate = reportDetail.endDate;
                                                            let astartDate = (sadate.getUTCFullYear()) + "-" + (sadate.getUTCMonth() + 1) + "-" + "01 00:00:00";

                                                            astartDate = new Date(astartDate).getTime();

                                                            var eayear = (eadate.getUTCFullYear());
                                                            var eamonth = (eadate.getUTCMonth() + 1);
                                                            var last_date = new Date(eayear, eamonth, 0).getDate();
                                                            let aendDate = eayear + "-" + eamonth + "-" + last_date + " 23:59:59";

                                                            aendDate = new Date(aendDate).getTime();

                                                            let boxCom = reports.filter(x => (x.BRICK_SERIAL_NUMBER == boxDt || x.BOX_SERIAL_NUMBER == boxDt) && (x.importedDate.getTime() > astartDate) && (x.importedDate.getTime() < aendDate));

                                                            for (var i = 0; i < boxCom.length; i++) {
                                                                totalsim.push(boxCom[i].BRICK_SERIAL_NUMBER);
                                                            }

                                                            agentD['totalSim'] += boxCom.length;
                                                            agentD['sumCom'] += boxCom.reduce(function(total, currentValue, currentIndex, array) {
                                                                return parseFloat(total) + parseFloat(currentValue.COMM_EXCL);
                                                            }, 0);
                                                        });

                                                        if ((ObjectId(agentD.supervisor).toString() == ObjectId(superV._id).toString()) && agentD['totalSim'] != 0) {

                                                            template = template + "<tr><td></td><td>" + agentD.name + "</td><td>" + agentD.contactnumber + "</td><td>" + agentD.bankName + "</td><td>" + agentD.paymentType + "</td><td>" + (parseFloat(agentD.totalSim) ? parseFloat(agentD.totalSim) : 0) + "</td><td>" + (parseFloat(agentD.sumCom) ? parseFloat(agentD.sumCom).toFixed(2) : 0) + "</td></tr>"
                                                            superVtotal = superVtotal + (parseFloat(agentD.sumCom) ? parseFloat(agentD.sumCom) : 0);
                                                        }
                                                    })
                                                    template = template + `<tr><td colspan='7'> ${superV.name} Total : ${superVtotal ? superVtotal.toFixed(2) : 0}</td></tr>`;
                                                    areaTotal = parseFloat(areaTotal + superVtotal);

                                                })

                                                template = template + `<tr><td colspan='7'> ${areaname.toString().toUpperCase()} Total : ${areaTotal ? areaTotal.toFixed(2) : 0}</td></tr>`;

                                            })
                                            console.log("10")

                                            let unbox = 'Unallocated';
                                            var newreport = reports.filter(item => totalsim.indexOf(item.BRICK_SERIAL_NUMBER) < 0)
                                            let ubFinalarray = [];
                                            template = template + `<tr><td colspan='7'></td></tr><tr><td colspan="7" class='font-weight-bold'> ${unbox.toString().toUpperCase()} : ${newreport.length}</td></tr>`
                                            template = template + "<tr><th></th><th>Box Id</th> <th>Phone No</th> <th>Bank Name</th> <th>Payment Type</th><th>Total Sims</th><th>Commission</th></tr>";

                                            uboxIdList['unallocateTotal'] = 0;
                                            console.log("11")
                                            uboxIdList.forEach(function(ubId, key) {
                                                let uboxCom = newreport.filter(x => (x.BRICK_SERIAL_NUMBER == ubId || x.BOX_SERIAL_NUMBER == ubId) && (x.importedDate.getTime() > uastartDate) && (x.importedDate.getTime() < uaendDate));
                                                if (uboxCom.length <= 0) return;
                                                uboxIdList['totalSim'] = uboxCom.length;
                                                uboxIdList['sumCom'] = uboxCom.reduce(function(total, currentValue, currentIndex, array) {
                                                    return parseFloat(total) + parseFloat(currentValue.COMM_EXCL);
                                                }, 0);
                                                uboxIdList['unallocateTotal'] += (uboxIdList['sumCom']);

                                                // if(key+1==uboxIdList.length){
                                                template = template + "<tr><td></td><td>" + ubId + "</td><td>Unallocated</td><td>Unallocated</td><td>Unallocated</td><td>" + (parseFloat(uboxIdList.totalSim) ? parseFloat(uboxIdList.totalSim) : 0) + "</td><td>" + (parseFloat(uboxIdList.sumCom) ? parseFloat(uboxIdList.sumCom).toFixed(2) : 0) + "</td></tr>";
                                                // }
                                                if (key + 1 == uboxIdList.length) {

                                                    template = template + `<tr><td colspan='7'> Unallocated packs Total : ${uboxIdList.unallocateTotal ? uboxIdList.unallocateTotal.toFixed(2) : 0}</td></tr>`;
                                                }
                                            })
                                            console.log("12")
                                            if (req.body.callcount == req.body.totalcall - 1) {

                                                template = template + "</tbody></table>"
                                            }

                                            return res.status(200).json({
                                                status: "Fetch All Data",
                                                code: 200,
                                                message: `Data Fetch Successfully`,
                                                data: template,
                                                error: []
                                            });
                                        })
                                    }
                                } else {
                                    console.log("13")
                                    var template = `<table class="table dataTable table-striped table-bordered table-sm commission-table" id="commission-table"> 
                                        <thead><tr><td colspan="7" class="bg-primary font-weight-bold">Date Range: ${req.body.startDate} -  ${req.body.endDate}</td></tr></thead><tbody>`;
                                    let unbox = 'Unallocated packs';
                                    let ubFinalarray = [];
                                    template = template + `<tr><td colspan='7'></td></tr><tr><td colspan="7" class='font-weight-bold'> ${unbox.toString().toUpperCase()} : ${uboxIdList.length}</td></tr>`
                                    template = template + "<tr><th></th><th>Box Id</th> <th>Phone No</th> <th>Bank Name</th> <th>Payment Type</th><th>Total Sims</th><th>Commission</th></tr>";

                                    uboxIdList['unallocateTotal'] = 0;
                                    uboxIdList.forEach(function(ubId, key) {
                                        let uboxCom = reports.filter(x => x.BOX_SERIAL_NUMBER == ubId && (x.importedDate.getTime() > uastartDate) && (x.importedDate.getTime() < uaendDate));
                                        uboxIdList['totalSim'] = uboxCom.length;
                                        uboxIdList['sumCom'] = uboxCom.reduce(function(total, currentValue, currentIndex, array) {
                                            return parseFloat(total) + parseFloat(currentValue.COMM_EXCL);
                                        }, 0);
                                        uboxIdList['unallocateTotal'] += (uboxIdList['sumCom']);

                                        // if(key+1==uboxIdList.length){
                                        template = template + "<tr><td></td><td>" + ubId + "</td><td>Unallocated</td><td>Unallocated</td><td>Unallocated</td><td>" + (parseFloat(uboxIdList.totalSim) ? parseFloat(uboxIdList.totalSim) : 0) + "</td><td>" + (parseFloat(uboxIdList.sumCom) ? parseFloat(uboxIdList.sumCom).toFixed(2) : 0) + "</td></tr>";
                                        // }
                                        if (key + 1 == uboxIdList.length) {

                                            template = template + `<tr><td colspan='7'> Unallocated packs Total : ${uboxIdList.unallocateTotal ? uboxIdList.unallocateTotal.toFixed(2) : 0}</td></tr>`;
                                        }
                                    })
                                    console.log("14")
                                    template = template + "</tbody></table>"

                                    return res.status(200).json({
                                        status: "Fetch All Data",
                                        code: 200,
                                        message: `Data Fetch Successfully`,
                                        data: template,
                                        error: []
                                    });
                                }
                            });
                        });
                    } else {
                        Bank.aggregate([{
                                    $lookup: {
                                        from: "stocks",
                                        localField: "name",
                                        foreignField: "bankName",
                                        as: "stocks"
                                    }
                                }
                                /*by using agent ,report ans user table*/
                                // {
                                //         $lookup: {
                                //             from: "agents",
                                //             localField: "name",
                                //             foreignField: "bankName",
                                //             as: "agents"
                                //         }
                                //     },
                                //     {
                                //         $replaceRoot: {
                                //             newRoot: {
                                //                 $mergeObjects: [{
                                //                     $arrayElemAt: ["$agents", 0]
                                //                 }, "$$ROOT"]
                                //             }
                                //         }
                                //     },
                                //     {
                                //         $project: {
                                //             agents: 0
                                //         }
                                //     },
                                //     {
                                //         "$addFields": {
                                //             "assignedBoxes": {
                                //                 "$ifNull": ["$assignedBoxes", []]
                                //             }
                                //         }
                                //     },
                                //     {
                                //         "$lookup": {
                                //             "from": "reports",
                                //             "localField": "assignedBoxes",
                                //             "foreignField": "BOX_SERIAL_NUMBER",
                                //             "as": "reportBoxes"
                                //         }
                                //     },
                                //     {
                                //         $lookup: {
                                //             from: "users",
                                //             localField: "supervisor",
                                //             foreignField: "_id",
                                //             as: "supervisor"
                                //         }
                                //     },
                            ])
                            .exec(function(err, bankDetail) {

                                if (err) {
                                    return res.status(400).json({
                                        status: "ERROR",
                                        code: 400,
                                        message: 'Bad Request',
                                        data: [],
                                        error: 'Bad Request'
                                    });
                                } else {
                                    let stockList = [],
                                        bankList = [],
                                        stockNewList = [];
                                    bankDetail.forEach(function(bankData) {
                                        bankList.push(bankData.name);
                                        bankData.stocks.forEach(function(stockArray) {
                                            let existData = stockList.filter(x => x == stockArray["kit sim"]);
                                            if (existData && existData.length == 0) {
                                                stockList.push(stockArray["kit sim"]);
                                                let checkBank = stockNewList.filter(item => item.bank == bankData.name);
                                                let getBankIndex = stockNewList.findIndex(item => item.bank == bankData.name);

                                                if (checkBank && checkBank.length == 0) {
                                                    stockNewList.push({
                                                        "bank": bankData.name,
                                                        "stock": [stockArray]
                                                    });
                                                }
                                                if (checkBank && checkBank.length > 0) {

                                                    stockNewList[getBankIndex].stock.push(stockArray);
                                                }
                                            }
                                        })
                                    })


                                    Report.find({
                                        BOX_SERIAL_NUMBER: {
                                            $in: stockList
                                        }
                                    }).exec(function(err, reportData) {

                                        stockNewList.forEach(function(sData) {

                                            sData.stock.forEach(function(stockData) {
                                                let existReport = reportData.filter(x => x.BOX_SERIAL_NUMBER == stockData["kit sim"]);
                                                if (existReport && existReport.length > 0) {


                                                    stockData["reportBoxes"] = existReport;
                                                }
                                            })

                                        })


                                        var template = `<table class="table dataTable comtable-responsive table-striped table-bordered table-sm commission-table" id="commission-table"><thead> <tr><td colspan="11" class="bg-primary font-weight-bold">Date Range: ${req.body.startDate} -  ${req.body.endDate}</td></tr></thead>`;

                                        var stockNewList_new = [];
                                        stockNewList.forEach(function(bankStockData, indexL) {
                                            stockNewList_new.push(bankStockData);
                                            let stock_new = [];
                                            bankStockData.stock.forEach(function(stockDetail, index) {
                                                var s_index = stock_new.findIndex(x => x.supervisorId.toString().trim() == bankStockData.stock[index].supervisorId.toString().trim());
                                                if (s_index < 0) {

                                                    stock_new.push(bankStockData.stock[index]);
                                                } else {
                                                    stock_new[s_index].reportBoxes = stock_new[s_index].reportBoxes.concat(bankStockData.stock[index].reportBoxes);
                                                }
                                            })
                                            stockNewList_new[indexL].stock = stock_new;
                                        });
                                        stockNewList = stockNewList_new;


                                        stockNewList.forEach(function(reportOfComm) {



                                            let total = 0;
                                            template = template + "<tr><td colspan='11'></td></tr><tr><td colspan='11' class='font-weight-bold'>Bank:" + reportOfComm.bank.toString().toUpperCase() + "</td></tr>";
                                            template = template + "<tbody><tr><th>Supervisor</th><th>Activation Date</th> <th>usage_spent_4mths</th> <th>usage_spent_7mths</th><th>voice_amt_4mths</th><th>gprsamt_4mths</th><th>gprs_inbundle_amt_4mths</th><th>sms_inbundle_amt_4mths</th><th>mms_inbundle_amt_4mths</th><th>voice_inbundle_amt_4mths</th><th>Commission</th></tr>";
                                            reportOfComm.stock.forEach(function(reportStock) {

                                                if (reportStock.reportBoxes && reportStock.reportBoxes.length > 0) {


                                                    let usage_spent_4mths = 0,
                                                        usage_spent_7mths = 0,
                                                        voice_amt_4mths = 0,
                                                        gprsamt_4mths = 0,
                                                        gprs_inbundle_amt_4mths = 0,
                                                        sms_inbundle_amt_4mths = 0,
                                                        mms_inbundle_amt_4mths = 0,
                                                        voice_inbundle_amt_4mths = 0,
                                                        commission = 0;
                                                    reportStock.reportBoxes.forEach(function(boxOfComm, index) {
                                                        let sdate = reportDetail.startDate;
                                                        let edate = reportDetail.endDate;
                                                        let startDate = (sdate.getUTCFullYear()) + "-" + (sdate.getUTCMonth() + 1) + "-" + "01 00:00:00";
                                                        startDate = new Date(startDate).getTime();

                                                        var eyear = (edate.getUTCFullYear());
                                                        var emonth = (edate.getUTCMonth() + 1);
                                                        var last_date = new Date(eyear, emonth, 0).getDate();
                                                        let endDate = eyear + "-" + emonth + "-" + last_date + " 23:59:59";
                                                        endDate = new Date(endDate).getTime();

                                                        var activationDate = new Date(boxOfComm.importedDate).getTime();

                                                        let actDate = new Date(boxOfComm.importedDate);
                                                        var ayear = (actDate.getUTCFullYear());
                                                        var amonth = (actDate.getUTCMonth() + 1);
                                                        var alast_date = actDate.getDate();
                                                        stMonth = stMonth <= 9 ? '0' + stMonth : stMonth;
                                                        //let aDate = (amonth <= 9 ? '0' + amonth : amonth) + "/" + (alast_date <= 9 ? '0' + alast_date : alast_date) + "/" + ayear;

                                                        totalsim.push(boxOfComm.BRICK_SERIAL_NUMBER);
                                                        if (startDate < activationDate && endDate > activationDate) {
                                                            usage_spent_4mths += parseFloat(boxOfComm.usage_spent_4mths);
                                                            usage_spent_7mths += parseFloat(boxOfComm.usage_spent_7mths);
                                                            voice_amt_4mths += parseFloat(boxOfComm.voice_amt_4mths);
                                                            gprsamt_4mths += parseFloat(boxOfComm.gprsamt_4mths);
                                                            gprs_inbundle_amt_4mths += parseFloat(boxOfComm.gprs_inbundle_amt_4mths);
                                                            sms_inbundle_amt_4mths += parseFloat(boxOfComm.sms_inbundle_amt_4mths);
                                                            mms_inbundle_amt_4mths += parseFloat(boxOfComm.mms_inbundle_amt_4mths);
                                                            voice_inbundle_amt_4mths += parseFloat(boxOfComm.voice_inbundle_amt_4mths);
                                                            commission += parseFloat(boxOfComm.COMM_EXCL);
                                                        }

                                                        if (index + 1 == reportStock.reportBoxes.length) {

                                                            template = template + `<tr><td> ${reportStock.supervisor} </td><td> ${boxOfComm.ACTIVATION_DATE} </td><td> ${usage_spent_4mths ? `${parseFloat(usage_spent_4mths).toFixed(2)}` : 0}    </td> <td> ${usage_spent_7mths ? `${parseFloat(usage_spent_7mths).toFixed(2)}` : 0} </td><td>  ${voice_amt_4mths ? `${parseFloat(voice_amt_4mths).toFixed(2)}` : 0}</td><td> ${gprsamt_4mths ? `${parseFloat(gprsamt_4mths).toFixed(2)}` : 0}</td><td>  ${gprs_inbundle_amt_4mths ? `${parseFloat(gprs_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td> ${sms_inbundle_amt_4mths ? `${parseFloat(sms_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td> ${mms_inbundle_amt_4mths ? `${parseFloat(mms_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td>  ${voice_inbundle_amt_4mths ? `${parseFloat(voice_inbundle_amt_4mths).toFixed(2)}` : 0}  </td><td> ${parseFloat(commission) ? `${parseFloat(commission).toFixed(2)}`:0} </td></tr>`;
                                                            // template = template + `<tr><td> ${reportStock.supervisor} </td><td> ${boxOfComm.ACTIVATION_DATE} </td><td> ${usage_spent_4mths ? `${usage_spent_4mths}` : 0}    </td> <td> ${usage_spent_7mths ? `${usage_spent_7mths}` : 0} </td><td>  ${voice_amt_4mths ? `${voice_amt_4mths}` : 0}</td><td> ${gprsamt_4mths ? `${gprsamt_4mths}` : 0}</td><td>  ${gprs_inbundle_amt_4mths ? `${gprs_inbundle_amt_4mths}` : 0}</td><td> ${sms_inbundle_amt_4mths ? `${sms_inbundle_amt_4mths}` : 0}</td><td> ${mms_inbundle_amt_4mths ? `${mms_inbundle_amt_4mths}` : 0}</td><td>  ${voice_inbundle_amt_4mths ? `${voice_inbundle_amt_4mths}` : 0}  </td><td> ${parseFloat(commission) ? `${parseFloat(commission).toFixed(2)}`:0} </td></tr>`;
                                                        }
                                                    })

                                                    total += parseFloat(commission);

                                                    template = template + `<tr><td colspan='11'>${reportStock.supervisor} Total :  ${commission ? commission.toFixed(2) : 0}</td></tr>`;
                                                }
                                            })
                                            total = parseFloat(total);

                                            template = template + `<tr><td colspan='11'>${reportOfComm.bank} Total :  ${total ? total.toFixed(2) : 0}</td></tr>`
                                        });

                                        let unbox = 'Unallocated';
                                        var newreport = reports.filter(item => totalsim.indexOf(item.BRICK_SERIAL_NUMBER) < 0)
                                        let ubFinalarray = [];
                                        template = template + `<tr><td colspan='11'></td></tr><tr><td colspan="11" class='font-weight-bold'> ${unbox.toString().toUpperCase()} : ${newreport.length}</td></tr>`
                                        template = template + "<tr><th>Box Id</th><th>Activation Date</th> <th>usage_spent_4mths</th> <th>usage_spent_7mths</th><th>voice_amt_4mths</th><th>gprsamt_4mths</th><th>gprs_inbundle_amt_4mths</th><th>sms_inbundle_amt_4mths</th><th>mms_inbundle_amt_4mths</th><th>voice_inbundle_amt_4mths</th><th>Commission</th></tr>";
                                        // template = template + "<tr><th></th><th>Box Id</th> <th>Phone No</th> <th>Bank Name</th> <th>Payment Type</th><th>Total Sims</th><th>Commission</th></tr>";

                                        let total = 0;

                                        console.log(newreport.length, "=-=--=-=-=-=-=")
                                        uboxIdList.forEach(function(ubId, key) {
                                            // let uboxCom = newreport.filter(x => (x.BRICK_SERIAL_NUMBER == ubId || x.BOX_SERIAL_NUMBER == ubId) && (x.importedDate.getTime() > uastartDate) && (x.importedDate.getTime() < uaendDate));
                                            let usage_spent_4mths = 0,
                                                usage_spent_7mths = 0,
                                                voice_amt_4mths = 0,
                                                gprsamt_4mths = 0,
                                                gprs_inbundle_amt_4mths = 0,
                                                sms_inbundle_amt_4mths = 0,
                                                mms_inbundle_amt_4mths = 0,
                                                voice_inbundle_amt_4mths = 0,
                                                commission = 0;
                                            let uboxCom = newreport.filter(x => x.BOX_SERIAL_NUMBER == ubId && (x.importedDate.getTime() > uastartDate) && (x.importedDate.getTime() < uaendDate));
                                            if (uboxCom.length <= 0) return;

                                            uboxCom.forEach(function(ubankBox, index) {

                                                // for (var i = 0; i < boxCom.length; i++) {

                                                // }
                                                // uboxIdList['totalSim'] = uboxCom.length;
                                                // uboxIdList['sumCom'] = uboxCom.reduce(function (total, currentValue, currentIndex, array) {
                                                //     return parseFloat(total) + parseFloat(currentValue.COMM_EXCL);
                                                // }, 0);
                                                // uboxIdList['unallocateTotal'] += (uboxIdList['sumCom']);


                                                if (uastartDate < ubankBox.importedDate.getTime() && uaendDate > ubankBox.importedDate.getTime()) {
                                                    usage_spent_4mths += parseFloat(ubankBox.usage_spent_4mths);
                                                    usage_spent_7mths += parseFloat(ubankBox.usage_spent_7mths);
                                                    voice_amt_4mths += parseFloat(ubankBox.voice_amt_4mths);
                                                    gprsamt_4mths += parseFloat(ubankBox.gprsamt_4mths);
                                                    gprs_inbundle_amt_4mths += parseFloat(ubankBox.gprs_inbundle_amt_4mths);
                                                    sms_inbundle_amt_4mths += parseFloat(ubankBox.sms_inbundle_amt_4mths);
                                                    mms_inbundle_amt_4mths += parseFloat(ubankBox.mms_inbundle_amt_4mths);
                                                    voice_inbundle_amt_4mths += parseFloat(ubankBox.voice_inbundle_amt_4mths);
                                                    commission += parseFloat(ubankBox.COMM_EXCL);
                                                }
                                                if (index + 1 == uboxCom.length) {


                                                    template = template + `<tr><td> ${ubankBox.BOX_SERIAL_NUMBER} </td><td> ${ubankBox.ACTIVATION_DATE} </td><td> ${usage_spent_4mths ? `${parseFloat(usage_spent_4mths).toFixed(2)}` : 0}    </td> <td> ${usage_spent_7mths ? `${parseFloat(usage_spent_7mths).toFixed(2)}` : 0} </td><td>  ${voice_amt_4mths ? `${parseFloat(voice_amt_4mths).toFixed(2)}` : 0}</td><td> ${gprsamt_4mths ? `${parseFloat(gprsamt_4mths).toFixed(2)}` : 0}</td><td>  ${gprs_inbundle_amt_4mths ? `${parseFloat(gprs_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td> ${sms_inbundle_amt_4mths ? `${parseFloat(sms_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td> ${mms_inbundle_amt_4mths ? `${parseFloat(mms_inbundle_amt_4mths).toFixed(2)}` : 0}</td><td>  ${voice_inbundle_amt_4mths ? `${parseFloat(voice_inbundle_amt_4mths).toFixed(2)}` : 0}  </td><td> ${parseFloat(commission) ? `${parseFloat(commission).toFixed(2)}`:0} </td></tr>`;
                                                }
                                            })
                                            total += parseFloat(commission);
                                            if (key + 1 == uboxIdList.length) {

                                                template = template + `<tr><td colspan='11'> Unallocated packs Total : ${total ? total.toFixed(2) : 0}</td></tr>`;
                                            }
                                        })

                                        template = template + "</tbody></table>";

                                        return res.status(200).json({
                                            status: "Fetch All Data",
                                            code: 200,
                                            message: `Data Fetch Successfully`,
                                            data: template,
                                            error: []
                                        });

                                    });

                                }
                            })
                    }
                })
            }
        })
    }

    exports.upload_commission_file = (req, res, next) => {

        // req.file.originalname:
        //  'Sample - SIMCI_JAN2020_Classic_Sure_Trading__Pty__L.xlsx',
        let reportDetail = {
            filterType: req.body.reportType,
            startDate: new Date(req.body.startDate),
            endDate: new Date(req.body.endDate),
            importedDate: new Date(req.body.impDate)
        }

        reportDetail.startDate.setDate(reportDetail.startDate.getDate() + 1)
        reportDetail.endDate.setDate(reportDetail.endDate.getDate() + 1)
        reportDetail.importedDate.setDate(reportDetail.importedDate.getDate() + 1)
        // reportDetail.startDate=reportDetail.startDate.setDate(reportDetail.startDate.getDate()+1);


        let uploadedFile = req.file.buffer;

        var workbook = XLSX.read(uploadedFile, {
            type: "buffer"
        });

        var sheet_name_list = workbook.SheetNames;

        var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]], {
            dateNF: 'DD-MM-YYYY',
            raw: false
        });

        // let serialAry = [];



        xlData.forEach(function(exclData) {
            // let existRecord = serialAry.filter(x => x.SERIAL_NUMBER == exclData.SERIAL_NUMBER);
            // if (existRecord && existRecord.length == 0) {
            //     serialAry.push(exclData.SERIAL_NUMBER);
            // }

            var formattedDate = exclData.ACTIVATION_DATE.toString();
            formattedDate = formattedDate.split("/");
            var day = formattedDate[1].length == 1 ? '0' + formattedDate[1] : formattedDate[1];
            var month = formattedDate[0].length == 1 ? '0' + formattedDate[0] : formattedDate[0];
            var year = formattedDate[2];
            var fullDate = month + "/" + day + "/" + year;

            exclData.ACTIVATION_DATE = fullDate;
            // exclData.ACTIVATION_MYEAR = month + "/" + year;
            exclData.importedDate = reportDetail.importedDate;
            // exclData.ACTIVATION_DATE.setDate(exclData.ACTIVATION_DATE.getDate() + 1);


        })

        let monthIndex = (reportDetail.importedDate.getMonth() + 1) <= 9 ? '0' + (reportDetail.importedDate.getMonth() + 1) : (reportDetail.importedDate.getMonth() + 1)

        Report.aggregate([{
                $project: {
                    year: {
                        $year: "$importedDate"
                    },
                    month: {
                        $month: "$importedDate"
                    }
                }
            },
            {
                $match: {
                    month: parseInt(monthIndex),
                    year: parseInt(reportDetail.importedDate.getFullYear())
                }
            }
        ]).exec(function(err, reportsData) {



            if (reportsData.length > 0) {

                let reportRemoveArray = [];
                reportsData.forEach(function(report) {
                    reportRemoveArray.push(report._id);
                })

                Report.remove({
                    _id: {
                        $in: reportRemoveArray
                    }
                }).then(function(message) {

                    Report.insertMany(xlData)
                        .then(function(respose) {


                            let body = req.file;
                            if (!body) {
                                return res.status(400).json({
                                    status: "ERROR",
                                    code: 400,
                                    message: 'Bad Request',
                                    data: [],
                                    error: 'Bad Request'
                                });
                            } else {

                                return res.status(200).json({
                                    status: 'Bulk Inserted',
                                    code: 200,
                                    message: `${xlData.length} Data Inserted Successfully`,
                                    data: [],
                                    error: []
                                });
                            }
                            // return res.status(200).json({
                            //     status: 'Bulk Inserted',
                            //     code: 200,
                            //     message: `${xlData.length} Data Inserted Successfully`,
                            //     data: [],
                            //     error: []
                            // });
                        })
                        .catch(function(err) {





                            return res.status(200).json({
                                status: 'Bulk Inserted',
                                code: 400,
                                message: `File Contains Duplicate Data`,
                                data: [],
                                error: []
                            });
                        })
                }).catch(function(err) {
                    return res.status(200).json({
                        status: 'Bulk Inserted',
                        code: 400,
                        message: `File Contains Duplicate Data`,
                        data: [],
                        error: []
                    });
                })
                // Report.remove(reportsData)
            }
            if (reportsData.length == 0) {
                Report.insertMany(xlData)
                    .then(function(respose) {

                        let body = req.file;
                        if (!body) {
                            return res.status(400).json({
                                status: "ERROR",
                                code: 400,
                                message: 'Bad Request',
                                data: [],
                                error: 'Bad Request'
                            });
                        } else {

                            return res.status(200).json({
                                status: 'Bulk Inserted',
                                code: 200,
                                message: `${xlData.length} Data Inserted Successfully`,
                                data: [],
                                error: []
                            });
                        }
                        // return res.status(200).json({
                        //     status: 'Bulk Inserted',
                        //     code: 200,
                        //     message: `${xlData.length} Data Inserted Successfully`,
                        //     data: [],
                        //     error: []
                        // });
                    })
                    .catch(function(err) {

                        return res.status(200).json({
                            status: 'Bulk Inserted',
                            code: 400,
                            message: `File Contains Duplicate Data, Unique ${err.result.nInserted} Data Added`,
                            data: [],
                            error: []
                        });
                    })
            }
            // else {
            //     return res.status(200).json({
            //         status: 'Bulk Inserted',
            //         code: 400,
            //         message: `File Contains Duplicate Data`,
            //         data: [],
            //         error: []
            //     });
            // }
            // if(reportsData.length>0){
            //     Report
            //     .updateMany({
            //         "_id": {
            //             $in: [reportsData]
            //         }
            //     }, {
            //         $set: {
            //             //"bankName": data.bankName
            //         }
            //     }, {
            //         upsert: true
            //     }).then(function (err, datass) {
            //     })
            // }
        });
    }

    exports.delete_all_report = (req, res, next) => {

        Report.deleteMany({}).exec(function(err, reports) {
            if (err) {
                return res.status(400).json({
                    status: "Error When Delete reports",
                    code: 400,
                    message: `Error When Delete reports`,
                    data: [],
                    error: []
                });
            } else {
                return res.status(200).json({
                    status: "Fetch All Data",
                    code: 200,
                    message: `All Reports Deleted`,
                    data: [],
                    error: []
                });
            }

        });


    }