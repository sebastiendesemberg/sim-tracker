const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mailsender = require("../mailsender/index");
const CryptoJS = require("crypto-js");
const fs = require('fs');
const _ = require('lodash');
const request = require('request');
const User = require("../models/user");
const Area = require("../models/area");
const Province = require('../models/province');

exports.user_signup = (req, res, next) => {
    User.findOne({
        email: req.body.email
    }).exec().then(user => {
        if (user) {
            return res.status(409).json({
                status: "ERROR",
                code: 409,
                message: "Email Already Registered!",
                data: [],
                error: []
            });
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        status: "ERROR",
                        code: 500,
                        message: "Internal Server Error",
                        data: [],
                        error: err
                    });
                } else {
                    req.body.password = hash;
                    const user = new User(req.body);
                    user.save().then(result => {
                        res.status(201).json({
                            status: "CREATED",
                            code: 201,
                            message: "User Created Successfully",
                            data: [],
                            error: []
                        });
                    })
                }
            });
        }
    });
};

exports.user_login = (req, res, next) => {
    User.findOne({
        email: req.body.email,
        $or: [{
            role: 'Super-Admin'
        }, {
            role: 'Super-Staff'
        }]
    }).exec().then(user => {
        if (!user) {
            return res.status(401).json({
                status: "ERROR",
                code: 401,
                message: "Username/Password Not Found!",
                data: [],
                error: []
            });
        }

        var bytes = CryptoJS.AES.decrypt(req.body.password.toString(), process.env.CRYPTO_ENCRYPT_KEY);
        var decPassword = bytes.toString(CryptoJS.enc.Utf8);

        if (decPassword.length <= 0) {
            return res.status(400).json({
                status: "ERROR",
                code: 400,
                message: "Format not valid",
                data: [],
                error: []
            });
        }

        bcrypt.hash(decPassword, 10, function(err, hash) {
            // Store hash in database                          
            bcrypt.compare(decPassword, user.password).then(function(resdata) {
                if (resdata) {
                    const token = jwt.sign({
                            email: user.email,
                            userId: user._id
                        },
                        process.env.JWT_KEY, {
                            expiresIn: "96h"
                        }
                    );
                    return res.status(200).json({
                        status: "OK",
                        code: 200,
                        message: "LOGIN SUCCESS",
                        data: {
                            token: token,
                            user: user
                        },
                        error: []
                    });
                } else {
                    res.status(401).json({
                        status: "ERROR",
                        code: 401,
                        message: "Username/Password Not Found!",
                        data: [],
                        error: []
                    });
                }
            });
        });
    })
};

exports.user_create = (req, res, next) => {
    var data = new User(req.body);
    User.create(data, function(err, mod) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: err.errmsg,
                data: [],
                error: err.errmsg
            });
        } else {

            Area.findOne({
                _id: mod.area
            }, function(err, area) {
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: err.errmsg,
                        data: [],
                        error: err.errmsg
                    });
                } else {
                    area.userCounts = area.userCounts + 1;
                    area.save();
                    return res.status(200).json({
                        status: "Created",
                        code: 200,
                        message: req.params.collection + ` Created Successfully`,
                        data: mod,
                        error: []
                    });
                }
            });
        }
    })
}

exports.user_update = (req, res, next) => {
    let id = req.body.id
    if (req.body._id) {
        id = req.body._id;
        delete req.body._id;
    }
    User.findById(id, function(err, thing) {
        if (err) {
            return res.status(401).json({
                status: "ERROR",
                code: 401,
                message: "User not found",
                data: [],
                error: err
            });
        }
        if (!thing) {
            return res.status(401).json({
                status: "ERROR",
                code: 401,
                message: "User not found",
                data: [],
                error: []
            });
        }
        var updated = _.assign(thing, req.body);
        if (!updated) {
            return res.status(401).json({
                status: "ERROR",
                code: 401,
                message: "User not found",
                data: [],
                error: []
            });
        }
        updated.save(function(err) {
            if (err) {
                if (err.name === 'MongoError' && err.code === 11000) {
                    return res.status(401).json({
                        status: "ERROR",
                        code: 401,
                        message: "Sequence type should be unique",
                        data: [],
                        error: []
                    });
                } else {
                    return res.status(401).json({
                        status: "ERROR",
                        code: 401,
                        message: "Error Occure",
                        data: [],
                        error: err
                    });
                }
            } else {
                return res.status(200).json({
                    status: "OK",
                    code: 200,
                    message: "User Updated",
                    data: [],
                    error: []
                });
            }
        });
    });
}

exports.getAllUSERDATA = (req, res, next) => {

    User.find({
        role: {
            $ne: 'Super-Admin'
        }
    }).exec().then(user => {
        if (user.length <= 0) {
            return res.status(400).json({
                status: "ERROR",
                code: 400,
                message: "User Not Found",
                data: [],
                error: []
            });
        }

        return res.status(200).json({
            status: "OK",
            code: 200,
            message: "User List Data",
            data: user,
            error: []
        });
    })
}

exports.Single_user_details = (req, res, next) => {

    User.findOne({
        _id: req.params.userId
    }).exec().then(user => {
        if (!user) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "User Not Found",
                data: [],
                error: []
            });
        }

        return res.status(200).json({
            status: "OK",
            code: 200,
            message: "User Details",
            data: user,
            error: []
        });
    })
};

exports.user_delete = (req, res, next) => {
    Area.findOne({
        _id: req.params.userId
    }, function(err, area) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: err.errmsg,
                data: [],
                error: err.errmsg
            });
        } else {
            area.userCounts = area.userCounts - 1;
            console.log("Area count", area.userCounts);

            area.save();
            User.deleteOne({
                _id: req.params.userId
            }).exec().then(result => {
                res.status(200).json({
                    status: "OK",
                    code: 200,
                    message: "User Deleted",
                    data: [],
                    error: []
                });
            })
        }
    });

};

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

exports.recover_pass = (req, res, next) => {
    User.findOne({
        email: req.body.email
    }).exec().then(user => {
        if (!user) {
            return res.status(401).json({
                status: "ERROR",
                code: 401,
                message: "User Not Exist!",
                data: [],
                error: []
            });
        }

        let temppass = makeid(10);
        bcrypt.hash(temppass, 10, (err, hash) => {
            if (err) {
                return res.status(500).json({
                    status: "ERROR",
                    code: 500,
                    message: "Internal Server Error",
                    data: [],
                    error: err
                });
            } else {
                user.password = hash;
                user.save();
                var mail_data = {
                    'replace_var': {
                        name: user.name,
                        pass: temppass
                    },
                    'send_to': req.body.email,
                    'subject': 'Forgot Password'
                };
                mailsender.send_mail('/forgot-password.html', mail_data, function(response) {
                    if (response.is_error) {
                        return res.status(500).json({
                            status: "ERROR",
                            code: 500,
                            message: "Internal Server Error",
                            data: [],
                            error: response.is_error
                        });
                    }
                    res.status(201).json({
                        status: "CREATED",
                        code: 201,
                        message: "We sent you mail with new password",
                        data: [],
                        error: []
                    });
                });
            }
        });
    })

}


exports.changePassword = (req, res, next) => {

    User.find({
        email: req.body.email
    }).exec().then(user => {
        if (user.length < 1) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "User Not Found",
                data: [],
                error: []
            });
        }


        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(403).json({
                    status: "ERROR",
                    code: 403,
                    message: "Current Password is invalid",
                    data: [],
                    error: []
                });
            }

            if (result) {

                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            status: "ERROR",
                            code: 500,
                            message: "Internal Server Error",
                            data: [],
                            error: err
                        });
                    } else {
                        User.update({
                                email: req.body.email
                            }, {
                                $set: {
                                    password: hash
                                }
                            })
                            .exec()
                            .then(result => {
                                return res.status(200).json({
                                    status: "OK",
                                    code: 200,
                                    message: "Password Changed Success",
                                    data: [],
                                    error: []
                                });
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }
                });
            } else {
                res.status(403).json({
                    status: "ERROR",
                    code: 403,
                    message: "Current Password is invalid",
                    data: [],
                    error: []
                });
            }
        });
    });
};