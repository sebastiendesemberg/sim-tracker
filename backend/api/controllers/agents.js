const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js");
const fs = require('fs');
const _ = require('lodash');
const Agent = require("../models/agent");
const User = require("../models/user");
const Stock = require("../models/stock");
var ObjectId = require('mongodb').ObjectId;


exports.search_box = (req, res, next) => {
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;


    sortBy = req.body.orderBy ? req.body.orderBy : {
        '_id': 1
    };
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;

    let search = req.query.searchBy;
    let searchData;
    let isBoxes = false;

    if (req.body.searchBy) {
        isBoxes = true;

        searchData = {

            "_id": ObjectId(req.params.aid),
            "assignedBoxes": {
                $in: searchBy
            }

        }
    } else {
        isBoxes = false;

        searchData = {
            "_id": ObjectId(req.params.aid)
        }

    }


    if (isBoxes) {
        Agent.find(searchData).exec(function(err, annos) {
            console.log(err);

            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: "Data Not Found",
                    data: [],
                    error: err
                });
            } else {
                if (annos.length > 0) {
                    if (annos[0].assignedBoxes.length > 0) {
                        let foundMatch = annos[0].assignedBoxes.filter(x => x == searchBy);

                        if (foundMatch.length > 0) {
                            return res.status(200).json({
                                status: "Fetch All Data",
                                code: 200,
                                message: `Data Fetch Successfully`,
                                data: foundMatch,
                                error: []
                            });
                        } else {
                            return res.status(200).json({
                                status: "Fetch All Data",
                                code: 200,
                                message: `Data Fetch Successfully`,
                                data: [],
                                error: []
                            });
                        }

                    }
                }
                // let agentAry = [];
                // let newArray = [];
                // annos.forEach(element => {
                //     agentAry.push(element._id);
                // });
                return res.status(200).json({
                    status: "Fetch All Data",
                    code: 200,
                    message: `Data Fetch Successfully`,
                    data: annos,
                    error: []
                });
                // Agent.find({
                //     "assignedBoxes": {
                //         $exists: true,
                //         $in: agentAry
                //     },
                // }).exec(function(err, agentData) {
                //     if (err) {
                //         return res.status(404).json({
                //             status: "ERROR",
                //             code: 404,
                //             message: "Data Not Found",
                //             data: [],
                //             error: err
                //         });
                //     } else {

                //         agentData.forEach(element => {
                //             for (let index = 0; index < element.assignedBoxes.length; index++) {
                //                 const agentBoxIds = element.assignedBoxes[index];

                //                 if (newArray.indexOf(agentBoxIds) < 0) {
                //                     newArray.push(agentBoxIds);
                //                 }
                //             }
                //         });


                //         if (newArray.length == 0) {

                //             filteredData = agentAry;
                //         } else {
                //             var filteredData = agentAry.filter(function(e) {
                //                 return newArray.indexOf(e) < 0;
                //             });
                //         }


                //         return res.status(200).json({
                //             status: "Fetch All Data",
                //             code: 200,
                //             message: `Data Fetch Successfully`,
                //             data: filteredData,
                //             error: []
                //         });
                //     }
                // })
            }





            // Area.populate(annos, { path: '_id' }, function(err, populatedData) {
            //     console.log("Poppulated data", populatedData);

            // });

        })
    } else {


        Agent.find(searchData).exec(function(err, annoss) {
            console.log(err);

            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: "Data Not Found",
                    data: [],
                    error: err
                });
            } else {


                return res.status(200).json({
                    status: "Fetch All Data",
                    code: 200,
                    message: `Data Fetch Successfully`,
                    data: annoss[0].assignedBoxes,
                    error: []
                });

            }





        })
    }
}

exports.box_post = (req, res, next) => {
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;

    sortBy = req.body.orderBy ? req.body.orderBy : {
        '_id': 1
    };
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;

    // let search = req.query.search;
    // let searchData;
    // if (search != '' && search != undefined) {
    //     searchData = {
    //         $and: [
    //             { "supervisorId": ObjectId(req.params.sid) },
    //             { "kit sim": search }
    //         ]
    //     }
    // } else {
    //     searchData = { "supervisorId": ObjectId(req.params.sid) }

    // }

    // let searchById = { 'supervisor': { $exists: true, $ne: "" }, "kit sim": searchBy };

    // Stock.aggregate([{
    //         "$match": searchById,
    //     },
    //     {
    //         $group: {
    //             _id: "$kit sim",
    //             stocks: {
    //                 $push: "$$ROOT"
    //             }
    //         }
    //     },
    //     {
    //         "$sort": sortBy
    //     },
    //     {
    //         "$skip": pageData
    //     },
    //     {
    //         "$limit": limit
    //     },
    // ]).exec(function(err, annos) {

    //     if (err) {
    //         return res.status(404).json({
    //             status: "ERROR",
    //             code: 404,
    //             message: "Data Not Found",
    //             data: [],
    //             error: err
    //         });
    //     } else {
    //         if (annos.length > 0) {
    //             console.log("in IIFIFIFIFIFIFI")
    //             let agentAry = [];
    //             let newArray = [];
    //             annos.forEach(element => {
    //                 agentAry.push(element._id);
    //             });

    //             Agent.find({
    //                 "assignedBoxes": {
    //                     $exists: true,
    //                     $in: agentAry
    //                 },
    //             }).exec(function(err, agentData) {
    //                 if (err) {
    //                     return res.status(404).json({
    //                         status: "ERROR",
    //                         code: 404,
    //                         message: "Data Not Found",
    //                         data: [],
    //                         error: err
    //                     });
    //                 } else {

    //                     agentData.forEach(element => {
    //                         for (let index = 0; index < element.assignedBoxes.length; index++) {
    //                             const agentBoxIds = element.assignedBoxes[index];

    //                             if (newArray.indexOf(agentBoxIds) < 0) {
    //                                 newArray.push(agentBoxIds);
    //                             }
    //                         }
    //                     });


    //                     if (newArray.length == 0) {

    //                         filteredData = agentAry;
    //                     } else {
    //                         var filteredData = agentAry.filter(function(e) {
    //                             return newArray.indexOf(e) < 0;
    //                         });
    //                     }


    //                     return res.status(200).json({
    //                         status: "Fetch All Data",
    //                         code: 200,
    //                         message: `Data Fetch Successfully`,
    //                         data: filteredData,
    //                         error: []
    //                     });
    //                 }
    //             })
    //         } else {
    console.log("in else")
    let searchById = { 'supervisor': { $exists: true, $ne: "" }, $or: [{ "kit box": searchBy }, { "kit sim": searchBy }] };

    // $or: [{ "kit box": searchBy }, { "kit sim": searchBy }]

    Stock.aggregate([{
            "$match": searchById,
        },
        {
            $group: {
                _id: "$kit box",
                stocks: {
                    $push: "$$ROOT"
                }
            }
        },
        {
            "$sort": sortBy
        },
        {
            "$skip": pageData
        },
        {
            "$limit": limit
        },
    ]).exec(function(err, annos) {

        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {


            let agentAry = [];
            let newArray = [];
            annos.forEach(element => {
                agentAry.push(element._id);
            });

            Agent.find({
                "assignedBrickes": {
                    $exists: true,
                    $in: agentAry
                },
            }).exec(function(err, agentData) {
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "Data Not Found",
                        data: [],
                        error: err
                    });
                } else {

                    agentData.forEach(element => {
                        for (let index = 0; index < element.assignedBrickes.length; index++) {
                            const agentBoxIds = element.assignedBrickes[index];

                            if (newArray.indexOf(agentBoxIds) < 0) {
                                newArray.push(agentBoxIds);
                            }
                        }
                    });


                    if (newArray.length == 0) {

                        filteredData = agentAry;
                    } else {
                        var filteredData = agentAry.filter(function(e) {
                            return newArray.indexOf(e) < 0;
                        });
                    }


                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: filteredData,
                        error: []
                    });
                }
            })

        }


    })

    //         }
    //     }
    // })
}

exports.get_box_detail = (req, res, next) => {

    let search = req.query.search;
    let searchData;
    if (search != '' && search != undefined) {
        searchData = {
            $and: [{
                    "supervisorId": ObjectId(req.params.sid)
                },
                {
                    "kit sim": search
                }
            ]
        }
    } else {
        searchData = {
            "supervisorId": ObjectId(req.params.sid)
        }
    }

    Stock.aggregate([{
            "$match": searchData
        },
        {
            $group: {
                _id: "$kit sim"
            }
        },
        {
            $sort: {
                _id: 1
            }
        }
    ]).exec(function(err, data) {

        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {
            let agentAry = [];
            let newArray = [];
            data.forEach(element => {
                agentAry.push(element._id);
            });

            Agent.find({
                "assignedBoxes": {
                    $exists: true,
                    $in: agentAry
                },
            }).exec(function(err, agentData) {
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "Data Not Found",
                        data: [],
                        error: err
                    });
                } else {

                    agentData.forEach(element => {
                        for (let index = 0; index < element.assignedBoxes.length; index++) {
                            const agentBoxIds = element.assignedBoxes[index];

                            if (newArray.indexOf(agentBoxIds) < 0) {
                                newArray.push(agentBoxIds);
                            }
                        }
                    });


                    if (newArray.length == 0) {

                        filteredData = agentAry;
                    } else {
                        var filteredData = agentAry.filter(function(e) {
                            return newArray.indexOf(e) < 0;
                        });
                    }


                    return res.status(200).json({
                        status: "Fetch All Data",
                        code: 200,
                        message: `Data Fetch Successfully`,
                        data: filteredData,
                        error: []
                    });
                }
            })
        }
    })

}

exports.assign_box = (req, res, next) => {
    console.log(req.body.assignedBoxes, "req.body.assignedBoxes");
    try {

        Agent.findOne({
            _id: ObjectId(req.body.agentId)
        }).exec(function(err, data) {
            if (err) {
                return res.status(401).json({
                    status: "ERROR",
                    code: 401,
                    message: "Data not found",
                    data: [],
                    error: []
                });
            } else {
                let message = 'No Data';
                // Stock.find({ "kit sim": { $in: req.body.assignedBoxes } }).exec(function(err, boxfound) {

                //     if (boxfound.length > 0) {
                //         message = 'Boxes';
                //         data.assignedBoxes = req.body.assignedBoxes;
                //     } else {
                //         Stock.find({ "kit box": { $in: req.body.assignedBoxes } }).exec(function(err, bricksfound) {
                //             if (bricksfound.length > 0) {
                //                 message = 'Brickes';
                //                 data.assignedBrickes = req.body.assignedBrickes;
                //             }
                //         });
                //     }
                //     data.save();
                // return false;


                data.assignedBoxes = req.body.assignedBoxes;

                data.save();
                try {
                    Stock.updateMany({ "kit box": { $in: req.body.assignedBoxes } }, {
                        $set: { "bankName": data.bankName }
                    }, {
                        upsert: true
                    }).then(function(err, datass) {

                    })
                } catch (e) {
                    print(e);
                }

                return res.status(200).json({
                    status: "OK",
                    code: 200,
                    message: "Boxes assigned to Agent",
                    data: [],
                    error: []
                });
                // })
            }
        });


        // const stock = Agent.findOneAndUpdate({
        //     _id: req.body.agentId
        // }, {
        //     $set: {
        //         "name": 'testName'
        //     }
        // }, {
        //     multi: true
        // });
        // // await stock.save();
        // if (!stock) {
        //     return res.status(401).json({
        //         status: "ERROR",
        //         code: 401,
        //         message: "User not found",
        //         data: [],
        //         error: []
        //     });
        // }
        // return res.status(200).json({
        //     status: "OK",
        //     code: 200,
        //     message: "Dispatch Updated",
        //     data: [],
        //     error: []
        // });



    } catch (err) {

        return res.status(500).json({
            status: "ERROR",
            code: 500,
            message: "Error in update",
            data: [],
            error: err
        });
    }
}

exports.get_agent_selected_box = (req, res, next) => {

    Agent.findOne({
        _id: ObjectId(req.params.aid)
    }).exec(function(err, annos) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {

            console.log("annos.assignedBoxes")
            console.log(annos.assignedBoxes)
            Stock.find({
                'kit box': { $in: annos.assignedBoxes }
            }).exec(function(err, bricksfound) {

                console.log("bricksfound-=-=-=-=-=-=")
                console.log(bricksfound[0])

                // Declare a new array 
                let newArray = [];

                // Declare an empty object 
                let uniqueObject = {};

                // Loop for the array elements 
                for (let i in bricksfound) {

                    // Extract the title 
                    objTitle = bricksfound[i]['kit box'];

                    // Use the title as the index 
                    uniqueObject[objTitle] = bricksfound[i];
                }

                // Loop to push unique object into array 
                for (i in uniqueObject) {
                    newArray.push(uniqueObject[i]);
                }

                var assingbx = annos.assignedBoxes;
                var finaldata = [];
                for (var i = 0; i < newArray.length; i++) {
                    let obj = {};
                    for (var j = 0; j < assingbx.length; j++) {
                        if (newArray[i]['kit box'] == assingbx[j]) {
                            obj.bricks = assingbx[j];
                            obj.box = newArray[i]['kit sim'];
                            finaldata.push(obj)
                        }
                    }
                }
                var datafinal = {
                    agent: annos,
                    dataarray: finaldata
                }
                return res.status(200).json({
                    status: "Fetch All Data",
                    code: 200,
                    message: `Data Fetch Successfully`,
                    data: datafinal,
                    error: []
                });
            });
            // console.log(annos);
            // for (let index = 0; index < annos.assignedBoxes.length; index++) {
            //     const agentBoxIds = element.assignedBoxes[index];

            //     if (newArray.indexOf(agentBoxIds) < 0) {
            //         newArray.push(agentBoxIds);
            //     }
            // }

        }
    })
}