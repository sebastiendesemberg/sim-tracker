const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const CryptoJS = require("crypto-js");
const fs = require('fs');
const _ = require('lodash');
const User = require("../models/user");
const Area = require("../models/area");
const Stock = require("../models/stock");
const Province = require("../models/province");
const async = require("async");
const XLSX = require("xlsx");
var ObjectId = require('mongodb').ObjectId;
exports.get = (req, res, next) => {
    var Model = '';

    if (req.params.collection == 'User') {
        Model = require("../models/user");
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
    }
    if (req.params.collection == 'Stock') {
        Model = require("../models/stock");
    }

    if (req.params.collection == 'Province') {
        Model = require("../models/province");
    }
    if (req.params.collection == 'Bank') {
        Model = require("../models/bank");
    }


    Model.find().exec(function(err, annos) {
        if (err) {
            return res.status(404).json({

                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {

            // console.log(annos)
            return res.status(200).json({
                status: "Fetch All Data",
                code: 200,
                message: `Data Fetch Successfully`,
                data: annos,
                error: []
            });
        }
    })
}


exports.post = (req, res, next) => {
    var Model = '';
    var sortBy = '';
    var searchBy = '';
    var pageData = 0;
    var limit = 100;

    sortBy = req.body.orderBy ? req.body.orderBy : {};
    searchBy = req.body.searchBy ? req.body.searchBy : {};
    pageData = req.body.pageData ? req.body.pageData : 0;
    console.log("Search data", searchBy, sortBy);

    if (req.params.collection == 'User') {
        Model = require("../models/user");
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
    }
    if (req.params.collection == 'Stock') {
        Model = require("../models/stock");
    }
    if (req.params.collection == 'Agent') {

        Model.find(searchBy).skip(pageData).limit(limit).sort({ name: 1 }).exec(function(err, annos) {
            console.log("annos")
            console.log(annos)
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: "Data Not Found",
                    data: [],
                    error: err
                });
            } else {
                Model.find(searchBy).count().exec(function(err, countData) {
                    if (err) {
                        return res.status(404).json({
                            status: "ERROR",
                            code: 404,
                            message: "Data Not Found",
                            data: [],
                            error: err
                        });
                    } else {


                        let supervisorIds = [];
                        let proviceNameIds = [];


                        if (annos.length > 0) {
                            annos.forEach(function(data) {
                                supervisorIds.push(data.supervisor);
                                proviceNameIds.push(data.province);
                            })
                        }


                        User.find({
                            _id: {
                                $in: supervisorIds
                            }
                        }).exec(function(err, supervisorData) {
                            if (err) {
                                return res.status(404).json({
                                    status: "ERROR",
                                    code: 404,
                                    message: "Data Not Found",
                                    data: [],
                                    error: err
                                });

                            }
                            Province.find({
                                _id: {
                                    $in: proviceNameIds
                                }
                            }).exec(function(err, provinceData) {

                                if (err) {
                                    return res.status(404).json({
                                        status: "ERROR",
                                        code: 404,
                                        message: "Data Not Found",
                                        data: [],
                                        error: err
                                    });
                                }


                                annos.forEach(function(data) {
                                    supervisorData.forEach(function(sData) {
                                        // console.log(ObjectId(sData._id))
                                        if (ObjectId(data.supervisor).toString() == ObjectId(sData._id).toString()) {
                                            // console.log('matched')
                                            data.supervisorName = sData.name;
                                            // console.log(' data',  data, sData.name)
                                        }
                                    });


                                    provinceData.forEach(function(pData) {
                                        if (ObjectId(data.province).toString() == ObjectId(pData._id).toString()) {
                                            data.provinceName = pData.name;
                                        }
                                    });
                                });
                                return res.status(200).json({
                                    status: "Fetch All Data",
                                    code: 200,
                                    message: `Data Fetch Successfully`,
                                    data: annos,
                                    totalCount: countData,
                                    error: []
                                });

                            });


                        });
                        // console.log(annos)
                        // let cnt = 0;
                        // let agentArry = [];
                        // async.eachSeries(annos, function (item, callback) {

                        //     Province.findOne({
                        //         _id: ObjectId(item.province)
                        //     }).exec(function (err, provinceData) {
                        //         item.proviceName = provinceData.name;

                        //         User.findOne({
                        //             _id: ObjectId(item.supervisor)
                        //         }).exec(function (err, superData) {
                        //             item.supervisorName = superData.name;

                        //             agentArry.push(item);
                        //             // console.log(agentArry);
                        //             if (annos.length == cnt) {
                        //                 console.log(annos);
                        //                 // console.log(agentArry);
                        //             }
                        //         })

                        //     })

                        //     cnt++;
                        //     callback(null);
                        // })

                    }
                });

            }
        })

    } else {
        Model.find(searchBy).skip(pageData).limit(limit).sort(sortBy).exec(function(err, annos) {
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: "Data Not Found",
                    data: [],
                    error: err
                });
            } else {


                Model.find(searchBy).count().exec(function(err, countData) {
                    if (err) {
                        return res.status(404).json({
                            status: "ERROR",
                            code: 404,
                            message: "Data Not Found",
                            data: [],
                            error: err
                        });
                    } else {
                        // console.log(annos)
                        return res.status(200).json({
                            status: "Fetch All Data",
                            code: 200,
                            message: `Data Fetch Successfully`,
                            data: annos,
                            totalCount: countData,
                            error: []
                        });
                    }
                });

            }
        })
    }
}

exports.getById = (req, res, next) => {
    var Model = '';
    if (req.params.collection == 'User') {
        Model = require("../models/user");
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
    }
    Model.findOne({
        _id: req.params.id
    }, function(err, annos) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: "Data Not Found",
                data: [],
                error: err
            });
        } else {
            return res.status(200).json({
                status: "Fetch Single Data",
                code: 200,
                message: `Data Fetch Successfully`,
                data: annos,
                error: []
            });
        }
    })
}

exports.createnew = (req, res, next) => {
    var Model = '';
    if (req.params.collection == 'User') {
        Model = require("../models/user");
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
    }
    var data = new Model(req.body);
    if (req.params.collection == 'User') {
        var bytes = CryptoJS.AES.decrypt(req.body.password.toString(), process.env.CRYPTO_ENCRYPT_KEY);
        var decPassword = bytes.toString(CryptoJS.enc.Utf8);

        bcrypt.hash(decPassword, 10, function(err, hash) {

            data.password = hash;
            Model.create(data, function(err, mod) {
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: err.errmsg,
                        data: [],
                        error: err.errmsg
                    });
                } else {
                    // if (req.params.collection == 'User') {
                    Area.findOne({
                        _id: mod.area
                    }, function(err, area) {
                        if (err) {
                            return res.status(404).json({
                                status: "ERROR",
                                code: 404,
                                message: err.errmsg.includes('duplicate') ? 'Record Already Exist' : 'Something Went Wrong, Please Check Value',
                                data: [],
                                error: err.errmsg
                            });
                        } else {
                            if (area.userCounts && area.userCounts.length > 0) {
                                var ara = (area.userCounts).toString()
                                var idx = ara.indexOf(mod._id);
                                if (idx < 0) {
                                    area.userCounts.push(mod._id);
                                }
                            } else {
                                area.userCounts = [];
                                area.userCounts.push(mod._id);
                            }

                            area.save();
                            return res.status(200).json({
                                status: "Created",
                                code: 200,
                                message: req.params.collection + ` Created Successfully`,
                                data: mod,
                                error: []
                            });
                        }
                    });
                    // } else {
                    //     return res.status(200).json({
                    //         status: "Created",
                    //         code: 200,
                    //         message: req.params.collection + ` Created Successfully`,
                    //         data: mod,
                    //         error: []
                    //     });
                    // }
                }
            });
        });
    } else if (req.params.collection == 'Agent') {

        Model.create(data, function(err, mod) {
            console.log("err-=-=-=-=-=-=-==-")
            console.log(err)
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: err.errmsg.includes('duplicate') ? 'Record Already Exist' : 'Something Went Wrong, Please Check Value',
                    data: [],
                    error: err.errmsg
                });
            } else {
                return res.status(200).json({
                    status: "Created",
                    code: 200,
                    message: req.params.collection + ` Created Successfully`,
                    data: mod,
                    error: []
                });
            }
        })

    } else {
        Model.create(data, function(err, mod) {
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: err.errmsg.includes('duplicate') ? 'Record Already Exist' : 'Something Went Wrong, Please Check Value',
                    data: [],
                    error: err.errmsg
                });
            } else {
                return res.status(200).json({
                    status: "Created",
                    code: 200,
                    message: req.params.collection + ` Created Successfully`,
                    data: mod,
                    error: []
                });
            }
        })
    }
}

exports.update = (req, res, next) => {
    // console.log(req.params.collection);

    var Model = '';
    if (req.params.collection == 'User') {
        Model = require("../models/user");
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
    }
    var id = req.params.id;
    if (!id) {
        id = req.body._id;
    }
    if (req.body._id) {
        delete req.body._id;
    }

    Model.findById(id, function(err, thing) {
        if (err) {
            return res.status(400).json({
                status: "ERROR",
                code: 400,
                message: `Not Found`,
                data: [],
                error: err
            });
        }

        if (!thing) {
            return res.status(400).json({
                status: "ERROR",
                code: 400,
                message: `Not Found`,
                data: [],
                error: err
            });
        }

        if (req.params.collection == 'User') {
            var str1 = thing.area.toString();
            var str2 = req.body.area.toString();
            // console.log(str1, str2)/

            if (str1 - str2 != 0) {
                Area.findOne({
                    _id: thing.area
                }, function(err, area) {
                    if (err) {
                        return res.status(404).json({
                            status: "ERROR",
                            code: 404,
                            message: err.errmsg,
                            data: [],
                            error: err.errmsg
                        });
                    } else {
                        if (area.userCounts && area.userCounts.length > 0) {
                            var ara = (area.userCounts).toString()
                            var idx = ara.indexOf(thing._id);
                            if (idx < 0) {} else {
                                area.userCounts.splice(idx, 1);
                            }
                        }
                        area.save();
                        Area.findOne({
                            _id: req.body.area
                        }, function(err, areasec) {
                            if (err) {
                                return res.status(404).json({
                                    status: "ERROR",
                                    code: 404,
                                    message: err.errmsg,
                                    data: [],
                                    error: err.errmsg
                                });
                            } else {
                                if (areasec.userCounts && areasec.userCounts.length > 0) {
                                    var ara = (areasec.userCounts).toString()
                                    var idx = ara.indexOf(thing._id);
                                    if (idx < 0) {
                                        areasec.userCounts.push(thing._id);
                                    }
                                } else {
                                    areasec.userCounts = [];
                                    areasec.userCounts.push(thing._id);
                                }

                                areasec.save();
                                var updated = _.assign(thing, req.body);
                                if (!updated) {
                                    return res.status(400).json({
                                        status: "ERROR",
                                        code: 400,
                                        message: `Not Found`,
                                        data: [],
                                        error: err
                                    });
                                }
                                updated.save(function(err) {
                                    if (err) {
                                        return res.status(400).json({
                                            status: "ERROR",
                                            code: 400,
                                            message: `Sequence type should be unique"`,
                                            data: [],
                                            error: err
                                        });
                                    } else {
                                        return res.status(200).json({
                                            status: 'Updated',
                                            code: 200,
                                            message: req.params.collection + ` Updated Successfully`,
                                            data: thing,
                                            error: []
                                        });

                                    }
                                })
                            }
                        });
                    }
                });
            }
        } else {
            var updated = _.assign(thing, req.body);
            // console.log("updated", updated)
            if (!updated) {
                return res.status(400).json({
                    status: "ERROR",
                    code: 400,
                    message: `Not Found`,
                    data: [],
                    error: err
                });
            }
            updated.save(function(err) {
                if (err) {
                    return res.status(400).json({
                        status: "ERROR",
                        code: 400,
                        message: `Sequence type should be unique"`,
                        data: [],
                        error: err
                    });
                } else {
                    return res.status(200).json({
                        status: 'Updated',
                        code: 200,
                        message: req.params.collection + ` Updated Successfully`,
                        data: thing,
                        error: []
                    });

                }
            })
        }
    });
}


exports.server_side_pagination = (req, res, next) => {

    const Model = req.params.collection;
    let data = req.body;

    var where_cond = req.body.where_cond;

    var query = Model.find(where_cond);

    query = query.sort({
        create_date: -1
    });

    query = query.skip(req.body.skip).limit(req.body.limit).exec(function(err, annos) {
        if (err !== null) {
            return res.status(400).json({
                status: "ERROR",
                code: 400,
                message: err,
                data: [],
                error: err
            });
        } else {
            Model.find(where_cond).count().exec(function(er_, cnt) {
                if (er_) {
                    reject(err);
                } else {
                    var temp = {
                        recordsTotal: cnt,
                        data: annos,
                        recordsFiltered: cnt
                    }
                    return res.status(200).json({
                        status: 'Updated',
                        code: 200,
                        message: `Server Side Pagination`,
                        data: annos,
                        error: []
                    });
                }
            })
        }
    })
};

exports.bulk_insert = (req, res, next) => {
    let uploadedFile = req.file.buffer;
    //console.log("uploadedFile", uploadedFile)
    var workbook = XLSX.read(uploadedFile, {
        type: "buffer"
    });
    //console.log("workbook", workbook)
    var sheet_name_list = workbook.SheetNames;
    //console.log("sheet_name_list", sheet_name_list)
    var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
    // console.log(xlData[1], "XlData");


    let body = req.file;
    if (!body) {
        return res.status(400).json({
            status: "ERROR",
            code: 400,
            message: 'Bad Request',
            data: [],
            error: 'Bad Request'
        });
    }

    //xlData.parse(JSON.stringify(xlData).replace(/\s(?=\w+":)/g, "_"));
    // console.log(xlData)

    // if (!(body.data.constructor === Array)) { 
    //     return res.status(400).json({
    //         status: "ERROR",
    //         code: 400,
    //         message: 'Please Send Array',
    //         data: [],
    //         error: 'Please Send Array'
    //     });
    // }
    var inserted_objs = [];
    const Model = req.params.collection;
    var checkDataExist = false;

    Stock.insertMany(xlData)
        .then(function(respose) {
            // console.log(respose)
            return res.status(200).json({
                status: 'Bulk Inserted',
                code: 200,
                message: `${xlData.length} Data Inserted Successfully`,
                data: [],
                error: []
            });
        })
        .catch(function(err) {
            console.log(err)
            return res.status(200).json({
                status: 'Bulk Inserted',
                code: 400,
                message: `File Contains Duplicate Data, Unique ${err.result.nInserted} Data Added`,
                data: [],
                error: []
            });
            /* Error handling */
        });

    // async.eachSeries(xlData, function (item, cb) {
    //     console.log(item)
    //     item.kit_sim = item["kit sim"];
    //     item.kit_box = item["kit box"];
    //     item.active = true;
    //     Stock.find({
    //             kit: {
    //                 $eq: item.kit
    //             }
    //         })
    //         .exec().then(user => {
    //             if (user.length <= 0) {
    //                 Stock.create(item, function (err, mod) {
    //                     if (err) {
    //                         console.log(err);
    //                     } else {
    //                         inserted_objs.push(mod);
    //                     }
    //                     cb(null);
    //                 });
    //             } else {
    //                 checkDataExist = true;
    //                 cb(null);
    //             }
    //         })


    // }, function () {
    //     var msg;
    //     if (checkDataExist) {
    //         msg = `Data Inserted Successfully but some are duplicated`;
    //     } else {
    //         msg = `Data Inserted Successfully`;
    //     }
    //     console.log("success");

    //     return res.status(200).json({
    //         status: 'Bulk Inserted',
    //         code: 200,
    //         message: msg,
    //         data: inserted_objs,
    //         error: []
    //     });
    // })
}


exports.doc_transfer = (req, res, next) => {
    const Model = req.params.collection;

    var data = new Model(req.body);
    Model.create(data, function(err, mod) {
        if (err) {
            return res.status(404).json({
                status: "ERROR",
                code: 404,
                message: err.errmsg,
                data: [],
                error: err.errmsg
            });
        } else {
            return res.status(200).json({
                status: "Created",
                code: 200,
                message: `Document Created Successfully`,
                data: mod,
                error: []
            });
        }
    })
}

exports.destroy = (req, res, next) => {
    var Model = '';
    if (req.params.collection == 'User') {
        Model = require("../models/user");
        Model.findOne({
            _id: req.params.id
        }).exec().then(result => {
            console.log("Result", result);

            Area.findOne({
                _id: result.area
            }, function(err, area) {
                if (err) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: err.errmsg,
                        data: [],
                        error: err.errmsg
                    });
                } else {
                    if (area && area.userCounts && area.userCounts.length > 0) {
                        var ara = (area.userCounts);
                        var idx = ara.indexOf(result._id);

                        if (idx < 0) {} else {
                            area.userCounts.splice(idx, 1);
                        }
                    }
                    if (area) {
                        area.save();
                    }
                    Model.deleteOne({
                        _id: req.params.id
                    }).exec().then(result => {
                        res.status(200).json({
                            status: "OK",
                            code: 200,
                            message: req.params.collection + ` Deleted Successfully`,
                            data: [],
                            error: []
                        });
                    })
                    if (req.params.collection != 'Area' && req.params.collection != 'User') {
                        Model.deleteOne({
                            _id: req.params.id
                        }).exec().then(result => {
                            res.status(200).json({
                                status: "OK",
                                code: 200,
                                message: req.params.collection + ` Deleted Successfully`,
                                data: [],
                                error: []
                            });
                        })
                    }
                }
            });
        });
    }
    if (req.params.collection == 'Agent') {
        Model = require("../models/agent");
        Model.findOne({
            _id: req.params.id
        }, function(err, agent) {
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: err.errmsg,
                    data: [],
                    error: err.errmsg
                });
            } else {
                let assignBox = agent.assignedBoxes;
                Model.deleteOne({
                    _id: req.params.id
                }).exec().then(result => {
                    Stock.find({ "kit box": { $in: agent.assignedBoxes } }, function(err, data) {

                        if (data.length > 0) {
                            Stock.updateMany({ "kit box": { $in: agent.assignedBoxes } }, {
                                $set: { "bankName": '' }
                            }, {
                                upsert: true
                            }).then(function(err, datass) {
                                if (err) {
                                    return res.status(200).json({
                                        status: "OK",
                                        code: 200,
                                        message: req.params.collection + ` Deleted Successfully`,
                                        data: [],
                                        error: []
                                    });
                                }

                                return res.status(200).json({
                                    status: "OK",
                                    code: 200,
                                    message: req.params.collection + ` Deleted Successfully`,
                                    data: [],
                                    error: []
                                });
                            })
                        } else {
                            return res.status(200).json({
                                status: "OK",
                                code: 200,
                                message: req.params.collection + ` Deleted Successfully`,
                                data: [],
                                error: []
                            });
                        }
                    });
                })
            }
        });
    }
    if (req.params.collection == 'Area') {
        Model = require("../models/area");
        Model.findOne({
            _id: req.params.id
        }, function(err, area) {
            if (err) {
                return res.status(404).json({
                    status: "ERROR",
                    code: 404,
                    message: err.errmsg,
                    data: [],
                    error: err.errmsg
                });
            } else {
                if (area.userCounts && area.userCounts.length > 0) {
                    return res.status(404).json({
                        status: "ERROR",
                        code: 404,
                        message: "You can't delete this area",
                        data: [],
                        error: "You can't delete this area",
                    });
                } else {
                    Model.deleteOne({
                        _id: req.params.id
                    }).exec().then(result => {
                        return res.status(200).json({
                            status: "OK",
                            code: 200,
                            message: req.params.collection + ` Deleted Successfully`,
                            data: [],
                            error: []
                        });
                    })
                }
                // area.save();
            }
        });
    }


};