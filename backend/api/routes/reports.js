const express = require("express");
const router = express.Router();

const ReportsController = require('../controllers/reports');
const checkAuth = require('../middleware/check-auth');
var multer = require("multer");
const upload = multer();

router.post("/getCommisionReport", ReportsController.generate_report);
router.post("/count/stock", ReportsController.generate_report_get_stock_count);
router.post("/deleteAll", ReportsController.delete_all_report);
router.put("/uploadCommission", upload.single('uploadFile'), ReportsController.upload_commission_file);
// router.get("/getAgentSelectedList/:aid/:sid",AgentsController.get_agent_selected_box)

module.exports = router;