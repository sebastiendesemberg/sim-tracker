const express = require("express");
const router = express.Router();

const StockController = require('../controllers/stocks');
const checkAuth = require('../middleware/check-auth');

router.post("/create_list", StockController.stock_post);
router.post("/brick_list", StockController.brick_post);
router.post("/pack_list", StockController.pack_post);
router.delete("/stock_delete", StockController.stock_delete);
router.put("/dispatch_update", StockController.update_dispatch);





module.exports = router;