const express = require("express");
const router = express.Router();

const UserController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');
router.post("/signup", UserController.user_signup);

router.post("/login", UserController.user_login);

// router.post("/forgotPassword", UserController.forgotPassword);

// router.post("/resetPassword", checkAuth, UserController.resetPassword);

router.post("/recoverPassword", UserController.recover_pass);

router.put("/create", UserController.user_create);

router.post("/changePassword", checkAuth, UserController.changePassword);

router.get("/:userId", checkAuth, UserController.Single_user_details);

router.get("/list/all", UserController.getAllUSERDATA);

router.post("/update", checkAuth, UserController.user_update);

router.delete("/:userId", checkAuth, UserController.user_delete);

// router.get("/province", UserController.getProvinceData);


module.exports = router;