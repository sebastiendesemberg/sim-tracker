const express = require("express");
const router = express.Router();

const AgentsController = require('../controllers/agents');
const checkAuth = require('../middleware/check-auth');

router.get("/getAgentBoxList/:aid/:sid", AgentsController.get_box_detail);
router.post("/getAgentBoxList/:aid", AgentsController.box_post);
router.put("/assignBoxToAgent", AgentsController.assign_box);
router.get("/getAgentSelectedList/:aid", AgentsController.get_agent_selected_box);
router.post("/searchAgentBox/:aid", AgentsController.search_box);

module.exports = router;